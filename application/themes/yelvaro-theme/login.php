<?php
use Concrete\Core\Attribute\Key\Key;
use Concrete\Core\Http\ResponseAssetGroup;

defined('C5_EXECUTE') or die('Access denied.');

$r = ResponseAssetGroup::get();
$r->requireAsset('javascript', 'underscore');
$r->requireAsset('javascript', 'core/events');

$activeAuths = AuthenticationType::getList(true, true);
$form = Loader::helper('form');

$active = null;
if ($authType) {
    $active = $authType;
    $activeAuths = array($authType);
}
$image = date('Ymd') . '.jpg';

/** @var Key[] $required_attributes */

$attribute_mode = (isset($required_attributes) && count($required_attributes));
$this->inc('elements/header.php');
?>

<main class="login-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php Loader::element('system_errors', array('format' => 'block', 'error' => $error, 'success' => $success, 'message' => $message)); ?>
                <h1>
                    <?php echo !$attribute_mode ? t('Login') : t('Required Attributes') ?>
                </h1>
                <div class="login-form">
                    <div class="visible-xs ccm-authentication-type-select form-group text-center">
                        <?php
                        if ($attribute_mode) {
                            ?>
                            <i class="fa fa-question"></i>
                            <span><?php echo t('Attributes') ?></span>
                        <?php
                        } else if (count($activeAuths) > 1) {
                            ?>
                            <select class="form-control">
                                <?php
                                foreach ($activeAuths as $auth) {
                                    ?>
                                    <option value="<?php echo $auth->getAuthenticationTypeHandle() ?>">
                                        <?php echo $auth->getAuthenticationTypeName() ?>
                                    </option>
                                <?php
                                }
                                ?>
                            </select>

                        <?php
                        }
                        ?>
                        <label>&nbsp;</label>
                    </div>
                </div>
                <div class="login-row">
                    <div class="types hidden">
                        <ul class="auth-types">
                            <?php
                            if ($attribute_mode) {
                                ?>
                                <li data-handle="required_attributes">
                                    <i class="fa fa-question"></i>
                                    <span><?php echo t('Attributes') ?></span>
                                </li>
                            <?php
                            } else {
                                /** @var AuthenticationType[] $activeAuths */
                                foreach ($activeAuths as $auth) {
                                    ?>
                                    <li data-handle="<?php echo $auth->getAuthenticationTypeHandle() ?>">
                                        <?php echo $auth->getAuthenticationTypeIconHTML() ?>
                                        <span><?php echo $auth->getAuthenticationTypeName() ?></span>
                                    </li>
                                <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="controls">
                        <?php
                        if ($attribute_mode) {
                            $attribute_helper = new Concrete\Core\Form\Service\Widget\Attribute();
                            ?>
                            <form action="<?php echo View::action('fill_attributes') ?>" method="POST">
                                <div data-handle="required_attributes"
                                     class="authentication-type authentication-type-required-attributes">
                                    <div class="ccm-required-attribute-form"
                                         style="height:340px;overflow:auto;margin-bottom:20px;">
                                        <?php
                                        foreach ($required_attributes as $key) {
                                            echo $attribute_helper->display($key, true);
                                        }
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <button
                                            class="btn btn-primary pull-right"><?php echo t('Submit') ?></button>
                                    </div>
                                </div>
                            </form>
                        <?php
                        } else {
                            /** @var AuthenticationType[] $activeAuths */

                            foreach ($activeAuths as $auth) {
                                ?>
                                <div data-handle="<?php echo $auth->getAuthenticationTypeHandle() ?>"
                                     class="authentication-type authentication-type-<?php echo $auth->getAuthenticationTypeHandle() ?>">
                                    <?php $auth->renderForm($authTypeElement ?: 'form', $authTypeParams ?: array()) ?>
                                </div>
                            <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php  $this->inc('elements/footer.php'); ?>
