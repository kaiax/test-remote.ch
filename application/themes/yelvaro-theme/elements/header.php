<?php defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header_top.php');

/* Check if user is SuperUser*/
$u = new User();
$isSuperUser = false;
if ($u instanceof User && $u->getUserID() == USER_SUPER_ID) {
    $isSuperUser = true;
}
?>

<header id="header">

    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <?php
                $a = new GlobalArea('Header Sidebar');
                if(!$isSuperUser){
                    $a->disableControls();
                }
                $a->display($c);
                ?>
            </div>
            <div class="col-sm-8">
                <?php
                $a = new GlobalArea('Header Default');
                if(!$isSuperUser){
                    $a->disableControls();
                }
                $a->display($c);
                ?>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <?php
            $a = new Area('Header Full Width');
            //$a->enableGridContainer();
            $a->setCustomTemplate('image', 'full_width_image.php');
            $a->display($c);
            ?>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php
                $a = new GlobalArea('Header Content Top');
                if(!$isSuperUser){
                    $a->disableControls();
                }
                $a->display($c);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?php
                $a = new Area('Header Content Bottom');
                $a->setAreaGridMaximumColumns(12);
                $a->display($c);
                ?>
            </div>
        </div>
    </div>

</header>
