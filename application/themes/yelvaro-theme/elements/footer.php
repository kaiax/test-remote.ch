<?php defined('C5_EXECUTE') or die("Access Denied.");
?>

<div class="container-fluid">
    <a href="#" id="scroll-to-top" class="hidden-print" style="display:none;">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 14 14">
            <polyline class="arrow" points="1 11 7 5 13 11"/>
        </svg>
    </a>
</div>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php
                $a = new Area('Footer Content Top');
                $a->display();
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?php
                $a = new GlobalArea('Footer Content Bottom');
                $a->display();
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <?php
                $a = new GlobalArea('Footer Sidebar');
                $a->display();
                ?>
            </div>
            <div class="col-sm-8">
                <?php
                $a = new GlobalArea('Footer Default');
                $a->display();
                ?>
            </div>
        </div>
    </div>
</footer>

<?php $this->inc('elements/footer_bottom.php');
