<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<!DOCTYPE html>
<html lang="<?= Localization::activeLanguage(); ?>">
<head>
    <!-- Required meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Render as latest IE version -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- prevent indexing webiste -->
    <meta name="robots" content="noindex">
    <!-- prevent only google search robots -->
    <meta name="googlebot" content="noindex">

    <!-- Required concrete5 header element -->
    <?php Loader::element('header_required', array('pageTitle' => $pageTitle));?>

    <?php
    // Add docs.css if it's a mockup page
    if($c->getPageTemplateHandle( ) == 'mockup') {
        echo $html->css('css/bootstrap/docs.min.css');
    }
    // salt css request with revision get parameter in order to enforce browsers to throw out the cached versions
    // whenever changes have been made.
    $rev = '';
    if(Config::get('deployment.current.endrev')){
        $rev = '?rev='.Config::get('deployment.current.endrev');
    }
    echo $html->css($this->getStylesheet('main.less').$rev);
    ?>

    <script>
    // Fix IE 10 on Windows Phone 8 Viewport
    // https://css-tricks.com/snippets/javascript/fix-ie-10-on-windows-phone-8-viewport/
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement('style');
            msViewportStyle.appendChild(
                document.createTextNode(
                    '@-ms-viewport{width:auto!important}'
                )
            )
            document.querySelector('head').appendChild(msViewportStyle);
        }
    </script>
</head>
<body>
    <div class="<?= $c->getPageWrapperClass(); ?>">
