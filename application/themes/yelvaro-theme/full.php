<?php defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

<main>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php Loader::element('system_errors', array('format' => 'block', 'error' => $error, 'success' => $success, 'message' => $message)); ?>
                <?php
                $a = new Area('Main Content');
                //$a->enableGridContainer();
                $a->setAreaGridMaximumColumns(12);
                $a->display($c);
                ?>
            </div>

            <div class="col-xs-12 col-md-6">
                <?php
                $leftSubContent = new Area('Left Sub Content');
                $leftSubContent->setAreaGridMaximumColumns(12);
                $leftSubContent->display($c);
                ?>
            </div>
            <div class="col-xs-12 col-md-6">
                <?php
                $rightSubContent = new Area('Left Sub Content');
                $rightSubContent->setAreaGridMaximumColumns(12);
                $rightSubContent->display($c);
                ?>
            </div>
        </div>
    </div>
</main>

<?php  $this->inc('elements/footer.php');
