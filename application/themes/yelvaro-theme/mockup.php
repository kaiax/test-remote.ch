<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header_top.php'); ?>
<header class="">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Atomic Bootstrap</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Atoms <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#colors">Colors</a></li>
                            <li><a href="#grid">Grid</a></li>
                            <li><a href="#typography">Typography</a></li>
                            <li><a href="#icons">Icons</a></li>
                            <li><a href="#buttons">Buttons</a></li>
                            <li><a href="#tables">Tables</a></li>
                            <li><a href="#inputs">Inputs</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Molecules <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#alerts">Alerts</a></li>
                            <li><a href="#well">Well</a></li>
                            <li><a href="#dropdown">Dropdown</a></li>
                            <li><a href="#jumbotron">Jumbotron</a></li>
                            <li><a href="#badges">Badges</a></li>
                            <li><a href="#labels">Labels</a></li>
                            <li><a href="#listgroup">List Group</a></li>
                            <li><a href="#media">Media</a></li>
                            <li><a href="#panels">Panels</a></li>
                            <li><a href="#tooltips">Tooltips</a></li>
                            <li><a href="#popovers">Popovers</a></li>
                            <li><a href="#progressbar">Progress Bar</a></li>
                            <li><a href="#thumbnails">Thumbnails</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">Organisms <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#breadcrumbs">Breadcrumbs</a></li>
                            <li><a href="#modal">Modal</a></li>
                            <li><a href="#navbar">Navbar</a></li>
                            <li><a href="#pagination">Pagination</a></li>
                            <li><a href="#share-this-page">Share this Page</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true" aria-expanded="false">Templates <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/login">Login</a></li>
                            <li><a href="/page-hot-found">Page not found</a></li>
                            <li><a href="/">Default Template</a></li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="http://getbootstrap.com/" target="_blank">Bootstrap 3.3.7</a></li>
                    <li><a href="http://fontawesome.io/" target="_blank">FontAwesome 4.2.0</a></li>
                    <li><a href="http://api.jquery.com/" target="_blank">JQuery 1.11.3</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>
</header>

<main class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <h1 id="atoms">Atoms</h1>
            <p>If atoms are the basic building blocks of matter, then the atoms of our interfaces serve as the
                foundational building blocks that comprise all our user interfaces. These atoms include basic
                HTML elements like form labels, inputs, buttons, and others that can’t be broken down any further
                without ceasing to be functional.
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <h2 id="colors">Colors</h2>

            <div class="bs-example">
                <div class="color-swatches">
                    <div class="color-swatch brand-primary" title="@brand-primary"></div>
                    <div class="color-swatch brand-secondary" title="@brand-secondary"></div>
                    <div class="color-swatch brand-third" title="@brand-third"></div>
                </div>
                <div class="color-swatches">
                    <div class="color-swatch brand-success" title="@brand-success"></div>
                    <div class="color-swatch brand-info" title="@brand-info"></div>
                    <div class="color-swatch brand-warning" title="@brand-warning"></div>
                    <div class="color-swatch brand-danger" title="@brand-danger"></div>
                </div>
                <div class="color-swatches">
                    <div class="color-swatch gray-darker" title="@gray-darker"></div>
                    <div class="color-swatch gray-dark" title="@gray-dark"></div>
                    <div class="color-swatch gray" title="@gray"></div>
                    <div class="color-swatch gray-light" title="@gray-light"></div>
                    <div class="color-swatch gray-lighter" title="@gray-lighter"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <h2 id="grid">Grid</h2>

            <div class="bs-example">
                <div class="row show-grid">
                    <div class="col-md-1">.col-md-1</div>
                    <div class="col-md-1">.col-md-1</div>
                    <div class="col-md-1">.col-md-1</div>
                    <div class="col-md-1">.col-md-1</div>
                    <div class="col-md-1">.col-md-1</div>
                    <div class="col-md-1">.col-md-1</div>
                    <div class="col-md-1">.col-md-1</div>
                    <div class="col-md-1">.col-md-1</div>
                    <div class="col-md-1">.col-md-1</div>
                    <div class="col-md-1">.col-md-1</div>
                    <div class="col-md-1">.col-md-1</div>
                    <div class="col-md-1">.col-md-1</div>
                </div>
                <div class="row show-grid">
                    <div class="col-md-8">.col-md-8</div>
                    <div class="col-md-4">.col-md-4</div>
                </div>
                <div class="row show-grid">
                    <div class="col-md-4">.col-md-4</div>
                    <div class="col-md-4">.col-md-4</div>
                    <div class="col-md-4">.col-md-4</div>
                </div>
                <div class="row show-grid">
                    <div class="col-md-6">.col-md-6</div>
                    <div class="col-md-6">.col-md-6</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <h2 id="typography">Typography</h2>

            <div class="bs-example ccm-content-block">
                <h1>Überschrift 1</h1>

                <p>normaler Text - Lorem ipsum dolor sit amet, consetetur sadipscing elitr Lorem ipsum dolor
                    sit amet, consetetur sadipscing elitrLorem ipsum dolor sit amet, consetetur sadipscing elitr</p>

                <h2>Überschrift 2</h2>

                <p>Eine Linie normaler Text - Lorem ipsum dolor sit amet, consetetur sadipscing elitr Lorem ipsum dolor
                    sit amet, consetetur sadipscing elitrLorem ipsum dolor sit amet, consetetur sadipscing elitr</p>

                <h3>Überschrift 3</h3>

                <p>Eine Linie normaler Text - Lorem ipsum dolor sit amet, consetetur sadipscing elitr Lorem ipsum dolor
                    sit amet, consetetur sadipscing elitrLorem ipsum dolor sit amet, consetetur sadipscing elitr</p>

                <h4>Überschrift 4</h4>

                <p>Eine Linie normaler Text - Lorem ipsum dolor sit amet, consetetur sadipscing elitr Lorem ipsum dolor
                    sit amet, consetetur sadipscing elitrLorem ipsum dolor sit amet, consetetur sadipscing elitr</p>

                <h5>Überschrift 5</h5>

                <p>Eine Linie normaler Text - Lorem ipsum dolor sit amet, consetetur sadipscing elitr Lorem ipsum dolor
                    sit amet, consetetur sadipscing elitrLorem ipsum dolor sit amet, consetetur sadipscing elitr</p>

                <h6>Überschrift 6</h6>

                <p>Eine Linie normaler Text - Lorem ipsum dolor sit amet, consetetur sadipscing elitr Lorem ipsum dolor
                    sit amet, consetetur sadipscing elitrLorem ipsum dolor sit amet, consetetur sadipscing elitr</p>

                <p class="lead">Lead Text - Lorem ipsum dolor sit amet, consetetur sadipscing elitr Lorem ipsum dolor
                    sit amet, consetetur sadipscing elitrLorem ipsum dolor sit amet, consetetur sadipscing elitr</p>

                <ul>
                    <li>Listenpunkt ohne Nummerierung<br>Zweite Zeile<br>Dritte Zeile
                    </li>
                    <li>Listenpunkt ohne Nummerierung</li>
                    <li>Listenpunkt ohne Nummerierung</li>
                    <li>Listenpunkt ohne Nummerierung</li>
                    <ul>
                        <li>Listenpunkt ohne Nummerierung</li>
                        <li>Listenpunkt ohne Nummerierung<br>Zweite Zeile<br>Dritte Zeile</li>
                        <ul>
                            <li>Listenpunkt ohne Nummerierung</li>
                            <li>Listenpunkt ohne Nummerierung</li>
                        </ul>
                    </ul>
                </ul>
                <ol>
                    <li>Listenpunkt mit Nummerierung</li>
                    <li>Listenpunkt mit Nummerierung<br>Zweite Zeile<br>Dritte Zeile</li>
                    <ol>
                        <li>Listenpunkt mit Nummerierung</li>
                        <li>Listenpunkt mit Nummerierung</li>
                        <li>Listenpunkt mit Nummerierung<br>Zweite Zeile<br>Dritte Zeile</li>
                        <ol>
                            <li>Listenpunkt mit Nummerierung</li>
                            <li>Listenpunkt mit Nummerierung<br>Zweite Zeile<br>Dritte Zeile</li>
                            <li>Listenpunkt mit Nummerierung</li>
                        </ol>
                    </ol>
                    <li>Listenpunkt mit Nummerierung</li>
                    <li>Listenpunkt mit Nummerierung</li>
                    <li>Listenpunkt mit Nummerierung</li>
                </ol>
                <hr/>
                <p>
                    Lorem sit amet, consetetur sadipscing elitr Lorem ipsum dolor sit amet,
                    <a href="/">Normaler Link</a> Lorem ipsum
                    <strong>Bold</strong> sit amet, consetetur
                    <em>Italic</em> sadipscing elitr Lorem ipsum dolor sit amet, consetetur sadipscing ipsum dolor sit amet, consetetur.
                </p>
                <blockquote>
                    Blockquote - Lorem ipsum dolor sit amet, consetetur sadipscing elitr Lorem ipsum dolor
                    sit amet, consetetur sadipscing elitrLorem ipsum dolor sit amet, consetetur sadipscing elitr
                    <footer>Zitiert von <cite title="Michael Häfliger">einem schlauen Mann</cite></footer>
                </blockquote>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <h2 id="buttons">Buttons</h2>

            <div class="bs-example" data-example-id="btn-variants">
                <button type="button" class="btn btn-default">Default</button>
                <button type="button" class="btn btn-primary">Primary</button>
                <button type="button" class="btn btn-primary" disabled>Disabled</button>
                <button type="button" class="btn btn-success">Success</button>
                <button type="button" class="btn btn-info">Info</button>
                <button type="button" class="btn btn-warning">Warning</button>
                <button type="button" class="btn btn-danger">Danger</button>
                <button type="button" class="btn btn-link">Link</button>
                <div class="btn-group" role="group" aria-label="...">
                    <button type="button" class="btn btn-default">Left</button>
                    <button type="button" class="btn btn-default">Middle</button>
                    <button type="button" class="btn btn-default">Right</button>
                </div>
                <!-- Single button -->
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>
                <!-- Split button -->
                <div class="btn-group">
                    <button type="button" class="btn btn-danger">Action</button>
                    <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <h2 id="tables">Tables</h2>

            <div class="bs-example">
                <div class="table-responsive">
                    <table class="table">
                        <caption>Optional table caption.</caption>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <h2 id="inputs">Inputs</h2>
            <div class="bs-example">
                <?php
                $a = new Area('Form Block Area');
                $a->display($c);
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <h1>Molecules</h1>

            <p>Molecules are groups of atoms bonded together and are the smallest fundamental
                units of a compound. These molecules take on their own properties and serve as the backbone
                of our design systems.
            </p>
            <h2 id="alerts">Alerts</h2>
            <div class="bs-example">
                <div class="alert alert-danger">Achtung gefahr!</div>
                <div class="alert alert-warning">Warnung!</div>
                <div class="alert alert-info">Info</div>
                <div class="alert alert-success">Erfolgreich!</div>
            </div>
            <h2 id="well">Well</h2>

            <div class="bs-example">
                <div class="well">
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores
                    et ea rebum.
                    Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor
                    sit amet,
                    consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed
                    diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                    no sea takimata
                    sanctus est Lorem ipsum dolor sit amet.
                </div>
            </div>
            <h2 id="dropdown">Dropdown</h2>

            <div class="bs-example">
                <div class="btn-group">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>
                <div class="btn-group dropup">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu2"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropup
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                </div>
            </div>
            <h2 id="jumbotron">Jumbotron</h2>

            <div class="bs-example">
                <div class="jumbotron">
                    <h1>Hello, world!</h1>

                    <p>This is a simple hero unit, a simple jumbotron-style component for calling extra attention to
                        featured content or information.</p>

                    <p><a class="btn btn-primary btn-lg" href="#" role="button">Klick!</a></p>
                </div>
            </div>
            <h2 id="labels">Labels</h2>

            <div class="bs-example" data-example-id="label-variants"><span class="label label-default">Default</span>
                <span class="label label-primary">Primary</span> <span class="label label-success">Success</span> <span
                    class="label label-info">Info</span> <span class="label label-warning">Warning</span> <span
                    class="label label-danger">Danger</span>
            </div>
            <h2 id="badges">Badges</h2>

            <div class="bs-example" data-example-id="simple-badges"><a href="#">Inbox <span class="badge">42</span></a>
                <br><br>
                <button class="btn btn-primary" type="button"> Messages <span class="badge">4</span></button>
            </div>
            <h2 id="list-group">List Group</h2>

            <div class="bs-example">
                <ul class="list-group">
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item">Morbi leo risus</li>
                    <li class="list-group-item">Porta ac consectetur ac</li>
                    <li class="list-group-item">Vestibulum at eros</li>
                </ul>
            </div>
            <h2 id="media">Media</h2>

            <div class="bs-example" data-example-id="default-media">
                <div class="media">
                    <div class="media-left"><a href="#"> <img alt="64x64" class="media-object"
                                                              data-src="holder.js/64x64"
                                                              src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTk4ZDA5NWI0NyB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1OThkMDk1YjQ3Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNCIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4="
                                                              data-holder-rendered="true"
                                                              style="width: 64px; height: 64px;"> </a></div>
                    <div class="media-body"><h4 class="media-heading">Media heading</h4> Cras sit amet nibh libero, in
                        gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio,
                        vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate
                        fringilla. Donec lacinia congue felis in faucibus.
                    </div>
                </div>
                <div class="media">
                    <div class="media-left"><a href="#"> <img alt="64x64" class="media-object"
                                                              data-src="holder.js/64x64"
                                                              src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTk4ZDA5NDgyMSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1OThkMDk0ODIxIj48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNCIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4="
                                                              data-holder-rendered="true"
                                                              style="width: 64px; height: 64px;"> </a></div>
                    <div class="media-body"><h4 class="media-heading">Media heading</h4> Cras sit amet nibh libero, in
                        gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio,
                        vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate
                        fringilla. Donec lacinia congue felis in faucibus.
                        <div class="media">
                            <div class="media-left"><a href="#"> <img alt="64x64" class="media-object"
                                                                      data-src="holder.js/64x64"
                                                                      src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTk4ZDA5OWVkOSB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1OThkMDk5ZWQ5Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNCIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4="
                                                                      data-holder-rendered="true"
                                                                      style="width: 64px; height: 64px;"> </a></div>
                            <div class="media-body"><h4 class="media-heading">Nested media heading</h4> Cras sit amet
                                nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.
                                Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum
                                nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <div class="media-body"><h4 class="media-heading">Media heading</h4> Cras sit amet nibh libero, in
                        gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio,
                        vestibulum in vulputate at, tempus viverra turpis.
                    </div>
                    <div class="media-right"><a href="#"> <img alt="64x64" class="media-object"
                                                               data-src="holder.js/64x64"
                                                               src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTk4ZDA5OTI3NCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1OThkMDk5Mjc0Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNCIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4="
                                                               data-holder-rendered="true"
                                                               style="width: 64px; height: 64px;"> </a></div>
                </div>
                <div class="media">
                    <div class="media-left"><a href="#"> <img alt="64x64" class="media-object"
                                                              data-src="holder.js/64x64"
                                                              src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTk4ZDA5OTI2OCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1OThkMDk5MjY4Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNCIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4="
                                                              data-holder-rendered="true"
                                                              style="width: 64px; height: 64px;"> </a></div>
                    <div class="media-body"><h4 class="media-heading">Media heading</h4> Cras sit amet nibh libero, in
                        gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio,
                        vestibulum in vulputate at, tempus viverra turpis.
                    </div>
                    <div class="media-right"><a href="#"> <img alt="64x64" class="media-object"
                                                               data-src="holder.js/64x64"
                                                               src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIHZpZXdCb3g9IjAgMCA2NCA2NCIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+PCEtLQpTb3VyY2UgVVJMOiBob2xkZXIuanMvNjR4NjQKQ3JlYXRlZCB3aXRoIEhvbGRlci5qcyAyLjYuMC4KTGVhcm4gbW9yZSBhdCBodHRwOi8vaG9sZGVyanMuY29tCihjKSAyMDEyLTIwMTUgSXZhbiBNYWxvcGluc2t5IC0gaHR0cDovL2ltc2t5LmNvCi0tPjxkZWZzPjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+PCFbQ0RBVEFbI2hvbGRlcl8xNTk4ZDA5YThkOCB0ZXh0IHsgZmlsbDojQUFBQUFBO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1mYW1pbHk6QXJpYWwsIEhlbHZldGljYSwgT3BlbiBTYW5zLCBzYW5zLXNlcmlmLCBtb25vc3BhY2U7Zm9udC1zaXplOjEwcHQgfSBdXT48L3N0eWxlPjwvZGVmcz48ZyBpZD0iaG9sZGVyXzE1OThkMDlhOGQ4Ij48cmVjdCB3aWR0aD0iNjQiIGhlaWdodD0iNjQiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSIxNCIgeT0iMzYuNSI+NjR4NjQ8L3RleHQ+PC9nPjwvZz48L3N2Zz4="
                                                               data-holder-rendered="true"
                                                               style="width: 64px; height: 64px;"> </a>
                    </div>
                </div>
            </div>
            <h2 id="panels">Panels</h2>

            <div class="bs-example">
                <div class="panel panel-default">
                    <div class="panel-heading">Panel heading without title</div>
                    <div class="panel-body">
                        Panel content
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Panel title</h3>
                    </div>
                    <div class="panel-body">
                        Panel content
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-body">
                        Panel content
                    </div>
                    <div class="panel-footer">Panel footer</div>
                </div>
            </div>
            <h2 id="tooltips">Tooltips</h2>
            <script>
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })
            </script>
            <div class="bs-example">
                <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left"
                        title="Tooltip on left">Tooltip on left
                </button>
                <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="top"
                        title="Tooltip on top">Tooltip on top
                </button>
                <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="bottom"
                        title="Tooltip on bottom">Tooltip on bottom
                </button>
                <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="right"
                        title="Tooltip on right">Tooltip on right
                </button>
            </div>
            <h2 id="popovers">Popovers</h2>
            <script>
                $(function () {
                    $('[data-toggle="popover"]').popover()
                })
            </script>
            <div class="bs-example">
                <button type="button" class="btn btn-lg btn-danger" data-toggle="popover" title="Popover title"
                        data-content="And here's some amazing content. It's very engaging. Right?">Click to toggle
                    popover
                </button>
            </div>
            <h2 id="progress-bars">Progress Bars</h2>
            <div class="bs-example">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                         aria-valuemax="100" style="width: 60%;">
                        60%
                    </div>
                </div>
            </div>
            <h2 id="thumbnails">Thumbnails</h2>
            <div class="bs-example">
                <div class="row">
                    <div class="col-md-3 col-xs-6"><a href="#" class="thumbnail"> <img alt="100%x180"
                                                                                       data-src="holder.js/100%x180"
                                                                                       src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTU5OGQ0MWRkMjkgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTk4ZDQxZGQyOSI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI2MSIgeT0iOTQuNSI+MTcxeDE4MDwvdGV4dD48L2c+PC9nPjwvc3ZnPg=="
                                                                                       data-holder-rendered="true"
                                                                                       style="height: 180px; width: 100%; display: block;">
                        </a></div>
                    <div class="col-md-3 col-xs-6"><a href="#" class="thumbnail"> <img alt="100%x180"
                                                                                       data-src="holder.js/100%x180"
                                                                                       src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTU5OGQ0MWQ2MmEgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTk4ZDQxZDYyYSI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI2MSIgeT0iOTQuNSI+MTcxeDE4MDwvdGV4dD48L2c+PC9nPjwvc3ZnPg=="
                                                                                       data-holder-rendered="true"
                                                                                       style="height: 180px; width: 100%; display: block;">
                        </a></div>
                    <div class="col-md-3 col-xs-6"><a href="#" class="thumbnail"> <img alt="100%x180"
                                                                                       data-src="holder.js/100%x180"
                                                                                       src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTU5OGQ0MjA2NWIgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTk4ZDQyMDY1YiI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI2MSIgeT0iOTQuNSI+MTcxeDE4MDwvdGV4dD48L2c+PC9nPjwvc3ZnPg=="
                                                                                       data-holder-rendered="true"
                                                                                       style="height: 180px; width: 100%; display: block;">
                        </a></div>
                    <div class="col-md-3 col-xs-6"><a href="#" class="thumbnail"> <img alt="100%x180"
                                                                                       data-src="holder.js/100%x180"
                                                                                       src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTU5OGQ0MWJjNTIgdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTk4ZDQxYmM1MiI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI2MSIgeT0iOTQuNSI+MTcxeDE4MDwvdGV4dD48L2c+PC9nPjwvc3ZnPg=="
                                                                                       data-holder-rendered="true"
                                                                                       style="height: 180px; width: 100%; display: block;">
                        </a></div>
                </div>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h1>Organisms</h1>
                <p>Organisms are groups of molecules joined together to form a relatively complex, distinct section of
                    an interface. Organisms can consist of similar and/or different molecule types. For example,
                    a masthead organism might consist of diverse components like a logo, primary navigation, search
                    form, and list of social media channels. But a “product grid” organism might consist of the same
                    molecule (possibly containing a product image, product title and price) repeated over and over
                    again.
                </p>
                <h2 id="breadcrumbs">Breadcrumbs</h2>
                <div class="bs-example">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Library</a></li>
                        <li class="active">Data</li>
                    </ol>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12">
                <h2>Modals</h2>
                <div class="bs-example bs-example-padded-bottom">
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                        Launch demo modal
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                                </div>
                                <div class="modal-body">
                                    ...
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <h2>Navbars</h2>
                <div class="bs-example" data-example-id="default-navbar">
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="collapsed navbar-toggle" data-toggle="collapse"
                                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><span
                                        class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span
                                        class="icon-bar"></span> <span class="icon-bar"></span></button>
                                <a href="#" class="navbar-brand">Brand</a></div>
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                                    <li><a href="#">Link</a></li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                            role="button" aria-haspopup="true" aria-expanded="false">Dropdown
                                            <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">One more separated link</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                <form class="navbar-form navbar-left">
                                    <div class="form-group"><input class="form-control" placeholder="Search"></div>
                                    <button type="submit" class="btn btn-default">Submit</button>
                                </form>
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="#">Link</a></li>
                                    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"
                                                            role="button" aria-haspopup="true" aria-expanded="false">Dropdown
                                            <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
                <h2 id="pagination">Pagination</h2>
                <div class="bs-example ccm-pagination-wrapper">
                    <ul class="pagination">
                        <li class="prev"><span>Zurück</span></li>
                        <li class="active"><a href="#">1 <span class="sr-only">(aktuell)</span></a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li class="next"><a href="#">Weiter</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <div class="bs-example">
                    <?php
                    $a = new Area('Filter List Area');
                    $a->display($c);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h2 id="share-this-page">Share this Page</h2>
                <div class="bs-example">
                    <?php
                    $a = new Area('Share This Page Area');
                    $a->display($c);
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <h1>Templates</h1>
                <p>Templates consist mostly of groups of organisms stitched together to form pages. It’s here where we
                    start to see the design coming together and start seeing things like layout in action. Templates
                    are very concrete and provide context to all these relatively abstract molecules and organisms.
                    Templates are also where clients start seeing the final design in place. In my experience working
                    with
                    this methodology, templates begin their life as HTML wireframes, but over time increase fidelity to
                    ultimately become the final deliverable.
                </p>
            </div>
        </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <a href="/login" class="btn btn-primary" target="_blank">Login</a>
            <a href="/page-not-found" class="btn btn-primary" target="_blank">Page Not Found</a>
            <a href="/" class="btn btn-primary" target="_blank">Default Template</a>
        </div>
    </div>

</main>
<?php $this->inc('elements/footer.php');
