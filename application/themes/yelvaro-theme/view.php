<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php'); ?>

<main class="container">
    <div class="row">
        <div id="sidebar" class="hidden-xs col-sm-4">
            <?php
            $a = new GlobalArea('Sidebar');
            $a->display($c);
            ?>
        </div>
        <div id="content" class="col-sm-12 col-sm-8">
            <?php Loader::element('system_errors', array('format' => 'block', 'error' => $error, 'success' => $success, 'message' => $message)); ?>
            <?php print $innerContent; ?>
        </div>
    </div>
</main>

<?php  $this->inc('elements/footer.php');
