var yelvaro = yelvaro || {};
yelvaro.theme = (function() {
    "use strict";

    return {
        init: function() {
            this.components.init();
        },
        components: {
            autonav: function() {
                /**
                 * @type Number scrollTop   The current scroll position before
                 *                          opening the XS-navigation.
                 */
                var scrollPosition = 0;

                $(window).on('resize', function() {
                    var $el = $('header .navbar.default-basic');

                    if ($el.hasClass('open') && $el.find('.navbar-toggle:hidden').length > 0) {
                        // Close the autonav if the window was enlarged from XS to > XS.
                        $el.find('.navbar-toggle').click();
                    }
                });

                $('.navbar')
                    .on('show.bs.collapse', function (e) {
                        // Remember the current scroll position.
                        scrollPosition = $(window).scrollTop();

                        // Add some CSS-classes to open the autonav.
                        $('.navbar-toggle').addClass('active');
                        $('.navbar').addClass('open');
                        // This class is needed to avoid scrolling the page
                        // content while the autonav is open.
                        $('html').addClass('scroll-lock');

                        if ($(this).hasClass('default-basic')) {
                            e.preventDefault();

                            // Avoid animated autonav. Just open it.
                            $($(this).find('[data-toggle="collapse"]')
                                    .data('target'))
                                    .toggleClass('in');
                        }
                    })
                    .on('hide.bs.collapse', function (e) {
                        // Remove the previously added classes to restore the
                        // closed autonav state.
                        $('.navbar-toggle').removeClass('active');
                        $('.navbar').removeClass('open');
                        $('html').removeClass('scroll-lock');

                        if ($(this).hasClass('default-basic')) {
                            e.preventDefault();

                            // Reset to the previous scroll position.
                            $(window).scrollTop(scrollPosition);
                            // Avoid animated autonav. Just close it.
                            $($(this).find('[data-toggle="collapse"]')
                                    .data('target'))
                                    .toggleClass('in');
                        }

//                        var $headerImg = $('.ccm-page #header .header-image');
//                        if ($headerImg.length > 0) {
//                            $headerImg.backstretch('resize');
//                        }
                    });
            },
            init: function() {
                this.autonav();
                this.scrollToTop();
            },
            overlay: {
                getId: function() {
                    return 'yelvaro-overlay-container';
                },
                hide: function() {
                    $('#' + this.getId()).hide();
                },
                show: function() {
                    $('#' + this.getId()).show();
                }
            },
            scrollToTop: function() {
                $(window).on('scroll', function () {
                    $(this).scrollTop() > 200
                            ? $('#scroll-to-top').fadeIn()
                            : $('#scroll-to-top').fadeOut();
                });
                $('#scroll-to-top').on('click', function () {
                    return $('html, body').animate({scrollTop: 0}, 600);
                });
            }
        }
    };
}(yelvaro));

$(function() {
    yelvaro.theme.init();
});
