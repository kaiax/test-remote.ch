<?php
defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header_top.php'); ?>

<main>
    <div class="container">
        <?php Loader::element('system_errors', array('format' => 'block', 'error' => $error, 'success' => $success, 'message' => $message)); ?>
        <?php
        $a = new Area('Main Content');
        $a->setAreaGridMaximumColumns(12);
        $a->display($c);
        ?>
    </div>
</main>

<?php  $this->inc('elements/footer_bottom.php');
