<?php

namespace Application\Theme\AtomicTheme;

use \Concrete\Core\Asset\AssetList;
use \Concrete\Core\Area\Layout\Preset\Provider\ThemeProviderInterface;
use \Concrete\Core\Page\Theme\Theme;

class PageTheme extends Theme implements ThemeProviderInterface
{

    protected $pThemeGridFrameworkHandle = 'bootstrap3';

    public function getThemeName()
    {
        return t('Custom Yelvaro Template');
    }

    public function getThemeDescription()
    {
        return t('Our Bootstrap 3.3.7 Theme template for Yelvaro Concrete5.7 Projects'
                . ' based on Atomic Design principles ;).');
    }

    public function registerAssets()
    {
        // Register our own version of the bootstrap framework in order to be
        // able to use the most recent version without interfering with the c5
        // UI elements that use the outdated version
        $themefolder = basename(__DIR__);
        $al = AssetList::getInstance();

        $al->register(
                'javascript',
                'atomic-bootstrap/affix',
                'themes/' . $themefolder . '/js/bootstrap/affix.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'atomic-bootstrap/alert',
                'themes/' . $themefolder . '/js/bootstrap/alert.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'atomic-bootstrap/button',
                'themes/' . $themefolder . '/js/bootstrap/button.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'atomic-bootstrap/carousel',
                'themes/' . $themefolder . '/js/bootstrap/carousel.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'atomic-bootstrap/collapse',
                'themes/' . $themefolder . '/js/bootstrap/collapse.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'atomic-bootstrap/dropdown',
                'themes/' . $themefolder . '/js/bootstrap/dropdown.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'atomic-bootstrap/modal',
                'themes/' . $themefolder . '/js/bootstrap/modal.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'atomic-bootstrap/tooltip',
                'themes/' . $themefolder . '/js/bootstrap/tooltip.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'atomic-bootstrap/popover',
                'themes/' . $themefolder . '/js/bootstrap/popover.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'atomic-bootstrap/scrollspy',
                'themes/' . $themefolder . '/js/bootstrap/scrollspy.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'atomic-bootstrap/tab',
                'themes/' . $themefolder . '/js/bootstrap/tab.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'atomic-bootstrap/transition',
                'themes/' . $themefolder . '/js/bootstrap/transition.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'backstretch',
                'themes/' . $themefolder . '/js/jquery.backstretch.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );
        $al->register(
                'javascript',
                'theme',
                'themes/' . $themefolder . '/js/theme.js',
                ['version' => '3.3.7', 'minify' => true, 'combine' => true]
        );

        $al->registerGroup(
                'atomic-bootstrap',
                [
                    ['javascript', 'atomic-bootstrap/affix'],
                    ['javascript', 'atomic-bootstrap/alert'],
                    ['javascript', 'atomic-bootstrap/button'],
                    ['javascript', 'atomic-bootstrap/carousel'],
                    ['javascript', 'atomic-bootstrap/collapse'],
                    ['javascript', 'atomic-bootstrap/dropdown'],
                    ['javascript', 'atomic-bootstrap/modal'],
                    ['javascript', 'atomic-bootstrap/tooltip'],
                    ['javascript', 'atomic-bootstrap/popover'],
                    ['javascript', 'atomic-bootstrap/scrollspy'],
                    ['javascript', 'atomic-bootstrap/tab'],
                    ['javascript', 'atomic-bootstrap/transition']
                ]
        );

        // Require some stuff that is used globally directly here. Keep in mind
        // to keep this list as short as possible and require specifically used
        // Assets in block/page/pagetype controllers if needed.
        $this->providesAsset('css', 'core/frontend/*');
        $this->providesAsset('css', 'bootstrap/*');
        $this->providesAsset('css', 'bootstrap/*');
        $this->providesAsset('javascript', 'bootstrap/*');
        $this->requireAsset('css', 'font-awesome');
        $this->requireAsset('javascript', 'jquery');
        $this->requireAsset('javascript', 'backstretch');
        $this->requireAsset('atomic-bootstrap');
        $this->requireAsset('javascript', 'picturefill');
        $this->requireAsset('javascript-conditional', 'html5-shiv');
        $this->requireAsset('javascript-conditional', 'respond');
        $this->requireAsset('javascript', 'theme');
    }

    public function getThemeBlockClasses()
    {
        return [];
    }

    public function getThemeAreaClasses()
    {
        return [];
    }

    public function getThemeDefaultBlockTemplates()
    {
        return [];
    }

    public function getThemeResponsiveImageMap()
    {
        return [
            'image_lg' => '1200px',
            'image_md' => '992px',
            'image_sm' => '0'
        ];
    }

    public function getThemeEditorClasses()
    {
        return [
            [
                'title' => t('Lead Text'),
                'menuClass' => '',
                'spanClass' => 'lead'
            ],
        ];
    }

    public function getThemeAreaLayoutPresets()
    {
        $presets = [
            [
                'handle' => 'six_cols',
                'name' => 'Sechsspaltig (MD)',
                'container' => '<div class="row"></div>',
                'columns' => [
                    '<div class="col-xs-6 col-sm-4 col-md-2"></div>',
                    '<div class="col-xs-6 col-sm-4 col-md-2"></div>',
                    '<div class="col-xs-6 col-sm-4 col-md-2"></div>',
                    '<div class="col-xs-6 col-sm-4 col-md-2"></div>',
                    '<div class="col-xs-6 col-sm-4 col-md-2"></div>',
                    '<div class="col-xs-6 col-sm-4 col-md-2"></div>',
                ],
            ],
            [
                'handle' => 'four_cols',
                'name' => 'Vierspaltig (MD)',
                'container' => '<div class="row"></div>',
                'columns' => [
                    '<div class="col-sm-6 col-md-3"></div>',
                    '<div class="col-sm-6 col-md-3"></div>',
                    '<div class="col-sm-6 col-md-3"></div>',
                    '<div class="col-sm-6 col-md-3"></div>',
                ],
            ],
            [
                'handle' => 'three_cols',
                'name' => 'Dreispaltig (MD)',
                'container' => '<div class="row"></div>',
                'columns' => [
                    '<div class="col-sm-12 col-md-4"></div>',
                    '<div class="col-sm-12 col-md-4"></div>',
                    '<div class="col-sm-12 col-md-4"></div>',
                ],
            ],
            [
                'handle' => 'one_col_offset',
                'name' => 'Mittig eingerückt (1 Spalte)',
                'container' => '<div class="row"></div>',
                'columns' => [
                    '<div class="col-md-10 col-md-offset-1"></div>',
                ],
            ],
            [
                'handle' => 'two_cols_offset',
                'name' => 'Mittig eingerückt (2 Spalten)',
                'container' => '<div class="row"></div>',
                'columns' => [
                    '<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2"></div>',
                ],
            ]
        ];

        return $presets;
    }

}
