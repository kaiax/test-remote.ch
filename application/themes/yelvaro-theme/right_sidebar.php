<?php defined('C5_EXECUTE') or die("Access Denied.");
$this->inc('elements/header.php');

/* Check if user is SuperUser*/
$u = new User();
$isSuperUser = false;
if ($u instanceof User && $u->getUserID() == USER_SUPER_ID) {
    $isSuperUser = true;
}
?>

<main>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-8">
                <?php Loader::element('system_errors', array('format' => 'block', 'error' => $error, 'success' => $success, 'message' => $message)); ?>
                <?php
                $a = new Area('Main Content');
                $a->setAreaGridMaximumColumns(12);
                $a->display($c);
                ?>
                <?php
                $a = new GlobalArea('Content Footer');
                if(!$isSuperUser){
                    $a->disableControls();
                }
                $a->setAreaGridMaximumColumns(12);
                $a->display($c);
                ?>
            </div>
            <div id="sidebar" class="hidden-xs col-sm-4">
                <?php
                $a = new Area('Sidebar');
                if(!$isSuperUser){
                    $a->disableControls();
                }
                $a->display($c);
                ?>
            </div>
        </div>
    </div>
</main>

<?php  $this->inc('elements/footer.php');
