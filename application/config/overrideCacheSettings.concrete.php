<?php
return array(
    // Activate Debuging mode
    'debug' => array(
        'display_errors' => true,
        'detail' => 'debug'
    ),
    // Deactivate Caching
    'cache' => array (
        'enabled' => false,
        'overrides' => false,
        'blocks' => false,
        'assets' => false,
        'theme_css' => false,
        'pages'=> false,
        'doctrine_dev_mode' => true
    ),
    // Save Environment in Config for later use
    'environment' => 'dev',
    // Generate a LESS Sourcemap
    'theme' => [
        'generate_less_sourcemap' => false,
    ],
);