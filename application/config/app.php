<?php

return array(
    /**
     * Route themes
     */
    'theme_paths'         => array(
        '/dashboard'        => 'dashboard',
        '/dashboard/*'      => 'dashboard',
        '/account'          => VIEW_CORE_THEME,
        '/account/*'        => VIEW_CORE_THEME,
        '/install'          => VIEW_CORE_THEME,
        '/page-not-found'   => VIEW_CORE_THEME,
        '/login'            => VIEW_CORE_THEME,
        '/register'         => VIEW_CORE_THEME,
        '/maintenance_mode' => VIEW_CORE_THEME,
        '/upgrade'          => VIEW_CORE_THEME
    ),
);