<?php
return array(
    // Activate Pretty URLs
    'seo' => array(
        'url_rewriting' => true,
        'url_rewriting_all' => true
    ),
    // Disable News Overlay
    'external' => array(
        'news_overlay' => false,
        'news' => false,
    ),
    // Disable Marketplace
    'marketplace' => array(
        'enabled' => false,
    ),
    // Deactivate Help Overlay
    'misc'              => array(
        'help_overlay'                  => false,
        // after login, redirect the user to homepage instead of the c5 welcome screen
        'login_redirect' => 'HOMEPAGE'
    ),
    // And Help Button
    'accessibility' => array(
        'display_help_system' => false,
    ),
    'user' => array(
        // Dont display the account menu
        'display_account_menu' => false,
        // Make Users register with email instead of username
        'registration' => array(
            'email_registration' => true,
        ),
    ),
    // Don't create retina thumbnails
    'file_manager' => [
        'images' => [
            'create_high_dpi_thumbnails' => false,
            'use_exif_data_to_rotate_images' => true,
            'preview_image_size' => 'full',
            'preview_image_popover' => true,
        ]
    ],
);