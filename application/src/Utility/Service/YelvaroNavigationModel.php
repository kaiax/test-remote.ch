<?php

namespace Application\Utility\Service;

use \InvalidArgumentException;
use \OutOfBoundsException;

/**
 * Model class for the Application\Utility\Service\Navigation class.
 */

class YelvaroNavigationModel
{
    const YELVARO_LINK_TEXT_TYPE_CUSTOM = 1;
    const YELVARO_LINK_TEXT_TYPE_CNAME = 2;

    /** @var    object */
    private $currentPage;
    /** @var    boolean */
    private $evalParents = true;
    /** @var    object      An existing c5 page */
    private $fixedPage;
    /** @var    boolean */
    private $isHomePageCounted = false;
    /** @var    string */
    private $linkText = '';
    /** @var    int */
    private $linkTextType = self::YELVARO_LINK_TEXT_TYPE_CUSTOM;
    /** @var    string      A HTML tag to be used as link wrapper. */
    private $linkTextWrapper = '';
    /** @var    int         The min level a page must be to have a back link. Min value: 1; Max value: 1000 */
    private $minLevel = 2;
    /** @var    string */
    private $paramsString = '';
    /** @var    boolean */
    private $setParamsFromRequest = false;

    public function __construct(array $options = [])
    {
        if (!empty($options)) {
            $this->setOptions($options);
        }
    }

    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    public function getEvalParents()
    {
        return $this->evalParents;
    }

    public function getFixedPage()
    {
        return $this->fixedPage;
    }

    public function getIsHomePageCounted()
    {
        return $this->isHomePageCounted;
    }

    public function getLinkText()
    {
        return $this->linkText;
    }

    public function getLinkTextType()
    {
        return $this->linkTextType;
    }

    public function getLinkTextWrapper()
    {
        return $this->linkTextWrapper;
    }

    public function getMinLevel()
    {
        return $this->minLevel;
    }

    public function getObjectVars()
    {
        return get_object_vars($this);
    }

    public function getParamsString()
    {
        return $this->paramsString;
    }

    public function getSetParamsFromRequest()
    {
        return $this->setParamsFromRequest;
    }

    public function setCurrentPage($page)
    {
        if (is_object($page) && intval($page->getCollectionID()) >= 1) {
            $this->currentPage = $page;
        }
    }

    public function setIsHomePageCounted($bool)
    {
        $this->isHomePageCounted = $bool;
    }

    public function setEvalParents($bool)
    {
        $this->evalParents = $bool ? true : false;
    }

    /**
     * Set the fixed Page Parameter.<br />
     * Use this method to use the submitted page as back link. Its name can also be used as the link content.
     *
     * @param   object      $fixedPage              An existing c5 page object.
     * @throws InvalidArgumentException
     */
    public function setFixedPage($fixedPage)
    {
        if (!is_object($fixedPage)) {
            $e = t(
                    'The fixed link needs to be a c5 page object, %s found.',
                    gettype($fixedPage)
            );

            throw new InvalidArgumentException($e);
        }
        $this->fixedPage = $fixedPage;
    }

    /**
     * Set the Link Text.<br />
     * Use this method to display a specific text of the link content.
     *
     * @param   string      $linkText
     * @throws InvalidArgumentException
     */
    public function setLinkText($linkText)
    {
        if (!is_string($linkText)) {
            $e = t(
                    'The link text type needs to be a string, %s found.',
                    gettype($linkText)
            );

            throw new InvalidArgumentException($e);
        }

        $this->linkText = $linkText;
        $this->setLinkTextType(self::YELVARO_LINK_TEXT_TYPE_CUSTOM);
    }

    public function setLinkTextType($linkTextType)
    {
        $allowedValues = [
            self::YELVARO_LINK_TEXT_TYPE_CUSTOM,
            self::YELVARO_LINK_TEXT_TYPE_CNAME
        ];

        if ((string)intval($linkTextType) != $linkTextType) {
            $e = t('The link text type needs to be a integer value.');

            throw new InvalidArgumentException($e);
        } elseif (!in_array(intval($linkTextType), $allowedValues)) {
            $e = t('The requested link text type does not exist.');

            throw new InvalidArgumentException($e);
        }

        $this->linkTextType = intval($linkTextType);
    }

    public function setLinkTextWrapper($wrapper)
    {
        if (!is_string($wrapper)) {
            $e = t(
                    'The parameter has to be a string. Submitted type: %s',
                    gettype($wrapper)
            );

            throw new InvalidArgumentException($e);
        } elseif (strpos($wrapper, '<') === false || strpos($wrapper, '>') === false) {
            $e = t('The submitted wrapper is not a valid HTML tag');

            throw new InvalidArgumentException($e);
        }

        $this->linkTextWrapper = $wrapper;
    }

    public function setMinLevel($minLevel)
    {
        if ((string)intval($minLevel) != $minLevel) {
            $e = t('The minLevel needs to be a number between 1 and 1000');

            throw new InvalidArgumentException($e);
        } elseif (intval($minLevel) < 1 || intval($minLevel) > 1000) {
            $e = t('The value needs to be between 1 and 1000.');

            throw new OutOfBoundsException($e);
        }

        $this->minLevel = intval($minLevel);
    }

    public function setSetParamsFromRequest($setParamsFromRequest)
    {
        $this->setParamsFromRequest = $setParamsFromRequest;
    }

    public function setOptions(array $options)
    {
        $variables = $this->getObjectVars();

        foreach ($variables as $key => $value) {
            if (isset($options[$key])) {
                $methodeName = 'set' . ucfirst($key);

                if (method_exists($this, $methodeName)) {
                    $this->$methodeName($options[$key]);
                }
            }
        }
    }

    public function setParamsString($paramsString)
    {
        $this->paramsString = $paramsString;
    }
}
