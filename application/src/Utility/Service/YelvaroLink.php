<?php
namespace Application\Utility\Service;

use InvalidArgumentException;
use URL;
use \Concrete\Core\Page\Page;

class YelvaroLink
{
    /** No link will be added. */
    const YELVARO_LINK_TYPE_NONE = 1;
    /** The link will surround the submitted content. */
    const YELVARO_LINK_TYPE_SURROND = 2;
    /** The link will be displayed as a button at the end of the submitted content. */
    const YELVARO_LINK_TYPE_BUTTON = 3;

    /** @var string             Button classes. */
    private $buttonClass;
    /** @var array              CSS classes for the button container */
    private $buttonContainerClasses;
    /** @var string             The HTML tag of the button container */
    private $buttonContainerTag;
    /** @var string             Default link text */
    private $defaultLinkText;
    /** @var boolean            True, if a button should be added to the content. */
    private $hasButton = true;
    /** @var array              Assoc array with an intern key and the attribute handles as value. */
    private $linkHandles = [
        'detail' => 'link_detail',
        'internal' => 'link_intern',
        'external' => 'link_web',
        'file' => 'link_file'
    ];
    /** @var string             The value of the link target attribute. */
    private $linkTarget;
    /** @var array              The link targets for every defined link type. */
    private $linkTargets = [
        'detail' => '',
        'internal' => '',
        'external' => '_blank',
        'file' => '_blank',
        'empty' => ''
    ];
    /** @var string             The button text */
    private $linkText = '';
    /** @var string             The attribute handle of the button text ovverride. */
    private $linkTextHandle = 'link_text';
    /** @var integer            One of the defined YELVARO_LINK_TEXT constants of this class. */
    private $linkType = self::YELVARO_LINK_TYPE_SURROND;
    /** @var Page               concrete5 page object */
    private $page;
    /** @var string             The value of the title attribute */
    private $title;

    /**
     * Creates an Instance of the YelvaroLink Class.
     *
     * @param   string  $defaultLinkText
     */
    public function __construct($defaultLinkText = null) {
        $this->defaultLinkText = is_null($defaultLinkText) ? t('Read more') : $defaultLinkText;
        $this->resetButtonClass();
    }
    /**
     * Adds CSS Classes to the existing one for the Button.
     *
     * @param   string      $additionalButtonClass
     * @throws  BadMethodCallException
     * @throws  InvalidArgumentException
     */
    public function addButtonClass($additionalButtonClass)
    {
        if (is_string($additionalButtonClass)) {
            if (empty($additionalButtonClass)) {
                $msg = t('The button class needs to be a not empty string, but '
                        . 'empty string found.');
                throw new BadMethodCallException($msg);
            } else {
                $buttonClass = $this->getButtonClass();

                if (!empty($buttonClass)) {
                    $buttonClassArr = explode(' ', $buttonClass);
                    $addClassArr = explode(' ', $additionalButtonClass);
                    $additionalButtonClassArr = array_merge($buttonClassArr, $addClassArr);
                    $additionalButtonClass = implode(' ', $additionalButtonClassArr);
                }

                $this->setButtonClass($additionalButtonClass);
            }
        } else {
            $msg = t(
                    'Wrong data type submitted. String needed, %s found.',
                    gettype($additionalButtonClass)
            );
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * Returns whetever the Button should be displayed or not.
     *
     * @return  boolean                     True if the button will be displayed.
     */
    public function getAddButton()
    {
        return $this->hasButton;
    }
    /**
     * Returns the Button HTML Code if a URL was defined. Also a surrounding
     * Container is added if the corresponding Tag was set.
     *
     * @return string                       The button string.
     */
    public function getButton()
    {
        $btn = '';
        $url = $this->getPriorityLinkFromAttributes();

        if (!empty($url)) {
            $btn = '<a href="'
                    . $url
                    . '"'
                    . $this->getButtonClassAttribute()
                    . $this->getTargetAttribute()
                    . '>'
                    . $this->getLinkText()
                    . '</a>';
        }

        $button = $this->getButtonContainer($btn);

        return $button;
    }
    /**
     * Returns the Class of the Button.
     *
     * @return  string
     */
    public function getButtonClass()
    {
        return $this->buttonClass;
    }
    /**
     * Returns the Class Attribute of the Button.
     *
     * @return  string
     */
    public function getButtonClassAttribute()
    {
        return empty($this->buttonClass) ? '' : ' class="' . $this->buttonClass . '"';
    }
    /**
     * Returns the Classes of the Button Container if defined.
     *
     * @return array
     */
    public function getButtonContainerClasses()
    {
        return $this->buttonContainerClasses;
    }
    /**
     * Returns the HTML Tag of the Button Container.
     *
     * @return string
     */
    public function getButtonContainerTag()
    {
        return $this->buttonContainerTag;
    }
    /**
     * Returns the Content.
     * The content will be expanded, if following conditions are met:
     * - <code>$this->getAddButton() === true</code>
     * - <code>$this->getLinkType() === self::YELVARO_LINK_TYPE_BUTTON</code>
     *
     * @param   string      $contentRaw         The content
     * @return  string
     */
    public function getContent($contentRaw = '')
    {
        $content = '';

        if ($this->getAddButton()) {
            $button = '';
            $linkType = $this->getLinkType();

            if ($linkType === self::YELVARO_LINK_TYPE_NONE || $linkType === self::YELVARO_LINK_TYPE_SURROND) {
                $button = '<p'
                        . $this->getButtonClassAttribute()
                        . '>'
                        . $this->getLinkText()
                        . '</p>';
            } elseif ($linkType === self::YELVARO_LINK_TYPE_BUTTON) {
                $button = $this->getButton();
            }
            $content = $contentRaw . $button;
        } else {
            $content = $contentRaw;
        }

        return $content;
    }
    /**
     * Returns the defined Link Handles.
     *
     * @return  array                           An assoc array with specific keys and the attribute handles as value.
     */
    public function getLinkHandles()
    {
        return $this->linkHandles;
    }
    /**
     * Returns the defined Link Targets.<br />
     * <br />
     * The used keys:
     * - detail
     * - internal
     * - external
     * - file
     * - empty
     *
     * @return array
     */
    public function getLinkTargets()
    {
        return $this->linkTargets;
    }
    /**
     * Returns the Link Text of the current Page.
     *
     * @param   boolean     $respectExisting    If true the link text will only
     *                                          return if a corresponding attribute
     *                                          has been defined. Otherwise an
     *                                          empty string will be returned.
     * @param   mixed       $page               The current concrete5 page. Leave it empty
     *                                          if the page has already been defined.
     * @return  string                          The link text will be the one found as attribute of the current page,
     *                                          or the link text defined in the current instance of this class.
     */
    public function getLinkText($respectExisting = false, $page = null)
    {
        $ltHandle = $this->getLinkTextHandle();

        if ($respectExisting) {
            $link = $this->getPriorityLinkFromAttributes($page);

            if (empty($link)) {
                return;
            }
        }

        if (isset($this->page) && $this->page->getAttribute($ltHandle)) {
            return $this->page->getAttribute($ltHandle);
        } elseif (empty($this->linkText)) {
            return $this->defaultLinkText;
        } else {
            return $this->linkText;
        }
    }
    /**
     * Returns the Link Text Attribute Handle.
     *
     * @return  string                          The defined link text attribute handle.
     */
    public function getLinkTextHandle()
    {
        return $this->linkTextHandle;
    }
    /**
     * Returns the Link Type.
     *
     * @return  integer                         The defined link type.
     */
    public function getLinkType()
    {
        return $this->linkType;
    }
    /**
     * Returns the current Page.
     *
     * @return  Page                            The current page which will be the base to retrieve the attribute values.
     */
    public function getPage()
    {
        return $this->page;
    }
    /**
     * Returns the Link specified by the Link Order, if defined.
     *
     * @param   Page        $page               The page with the attributes to retrieve.
     * @return  string                          The link path.
     * @throws InvalidArgumentException
     */
    public function getPriorityLinkFromAttributes($page = null)
    {
        if (is_object($page)) {
            $this->setPage($page);
        }

        $page = $this->getPage();
        $linkHandles = array_reverse($this->getLinkHandles(), true);
        $link = '';

        if (!is_object($page)) {
            throw new InvalidArgumentException(t('Undefined page object.'));
        }

        foreach ($linkHandles as $type => $handle) {
            $attr = $page->getAttribute($handle);

            if ($type === 'file') {
                if (is_object($attr)) {
                    $link = $attr->getApprovedVersion()->getRelativePath();
                    $this->setLinkTargetByType($type);
                    break;
                }
            } elseif ($type === 'external') {
                if (!empty($attr)) {
                    $link = empty(parse_url($attr, PHP_URL_SCHEME)) ? '//' . $attr : $attr;
                    $this->setLinkTargetByType($type);
                    break;
                }
            } elseif ($type === 'internal') {
                $this->setLinkTargetByType($type);

                if (is_object($attr)) {
                    $link = $attr->getURL();
                    break;
                } elseif (! empty($attr)) {
                    $c = Page::getByID($attr);
                    $link = URL::to($c) . '';
                    break;
                }
            } elseif ($type === 'detail') {
                if ($attr) {
                    $link = URL::to($page) . '';
                    $this->setLinkTargetByType($type);
                    break;
                }
            } else {
                if (!empty($attr)) {
                    $link = $attr;
                    $this->setLinkTargetByType('empty');
                    break;
                }
            }
        }

        return $link;
    }
    /**
     * Returns the submitted Content surrounded by the specified Link, if defined.
     *
     * @param   Page        $page               The page with the defined link attributes.
     * @param   string      $content            The content wich will be surrounded by a link.
     * @return  string
     */
    public function getPriorityLinkTagFromAttributes($page = null, $content = null)
    {
        $path = $this->getPriorityLinkFromAttributes($page);
        $linkType = $this->getLinkType();

        if (empty($path) || $linkType === self::YELVARO_LINK_TYPE_NONE) {
            return !is_null($content) ? $content :  '';
        } else {
            if ($linkType === self::YELVARO_LINK_TYPE_SURROND) {
                return '<a href="'
                        . $path
                        . '"'
                        . $this->getButtonClassAttribute()
                        . $this->getTargetAttribute()
                        . $this->getTitleAttribute()
                        . '>'
                        . $this->getContent($content)
                        . '</a>';
            } elseif ($linkType === self::YELVARO_LINK_TYPE_BUTTON) {
                return $this->getContent($content);
            }
        }
    }
    /**
     * Returns the Target Value.
     *
     * @return  string                          The defined target attribute value.
     */
    public function getLinkTarget()
    {
        return $this->linkTarget;
    }
    /**
     * Returns the Target Attribute.
     *
     * @return  string                          The target attribute string.
     */
    public function getTargetAttribute()
    {
        return empty($this->getLinkTarget()) ? '' : ' target="_blank"';
    }
    /**
     * Returns the Value of the Title Attribute.
     *
     * @return  string                          The defined title attribute value.
     */
    public function getTitle()
    {
        return $this->title;
    }
    /**
     * Returns the Title Attribute String.
     *
     * @return  string                          The title attribute string.
     */
    public function getTitleAttribute()
    {
        return empty($this->getTitle()) ? '' : ' title="' . $this->getTitle() . '"';
    }
    /**
     * Resets the Button CSS Classes to their default Value.
     */
    public function resetButtonClass()
    {
        $this->buttonClass = 'btn btn-default';
    }
    /**
     * Resets all link Handles by the values of the submitted Array.
     *
     * @param   array       $linkHandles        Assoc array with the types as key and the attribute handles as values.
     * @throws InvalidArgumentException
     */
    public function resetLinkHandles(array $linkHandles)
    {
        if (!empty($linkHandles)) {
            $this->linkHandles = $linkHandles;
        } else {
            throw new InvalidArgumentException(t('Empty array found.'));
        }
    }
    /**
     * Controlls if a Button should be displayed or not.
     *
     * @param   boolean     $hasButton              True if a button should be displayed.
     */
    public function setAddButton($hasButton = true)
    {
        $this->hasButton = $hasButton ? true : false;
    }
    /**
     * Sets the CSS Classes of the Button.<br />
     * Using this method will override existing classes.
     *
     * @param   string      $buttonClass            The new CSS classes for the button.
     * @throws  InvalidArgumentException
     */
    public function setButtonClass($buttonClass = '')
    {
        if (is_string($buttonClass)) {
            $this->buttonClass = $buttonClass;
        } else {
            $msg = t(
                    'Wrong data type submitted. String needed, %s found.',
                    gettype($buttonClass)
            );
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * Sets the Button Container Tag and the corresponding CSS Classes.<br />
     * <br />
     * This method calls the following methods:
     * - <code>$this->setButtonContainerClasses()</code>
     * - <code>$this->setButtonContainerTag()</code>
     *
     * @param string $tag
     * @param mixed $classes
     */
    public function setButtonContainer($tag, $classes = [])
    {
        $this->setButtonContainerTag($tag);
        $this->setButtonContainerClasses($classes);
    }
    /**
     * Sets the CSS Classes of the Button Container.<br />
     * <br />
     * The classes must be an array of CSS classes as strings. If the classes
     * are submitted as a string, the classes must be separated by comma.
     *
     * @param mixed $classes
     * @throws InvalidArgumentException
     */
    public function setButtonContainerClasses($classes)
    {
        if (is_string($classes)) {
            $this->buttonContainerClasses = explode(',', $classes);
        } elseif (is_array($classes)) {
            $this->buttonContainerClasses = $classes;
        } else {
            $msg = t('The button container classes parameter needs to be '
                    . 'a string or an array, found "%s"', gettype($classes));
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * Sets the HTML Tag of the Button Container.<br />
     *
     * @param string $tag
     * @throws InvalidArgumentException
     */
    public function setButtonContainerTag($tag)
    {
        if (!is_string($tag) && !is_null($tag)) {
            $msg = t('The button tag parameter needs to be a string, '
                    . 'found "%s"', gettype($tag));
            throw new InvalidArgumentException($msg);
        }

        $this->buttonContainerTag = $tag;
    }
    /**
     * Sets a Link Handle by Type.
     *
     * @param   string      $type               Valid types: "detail", "internal", "external", "file".
     * @param   string      $handle             The new attribute handle of the submitted type.
     * @throws InvalidArgumentException
     */
    public function setLinkHandle($type, $handle)
    {
        if (in_array($type, array_keys($this->linkHandles))) {
            $this->linkHandles[$type] = $handle;
        } else {
            $msg = t('The submitted link type does not exist.');
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * Sets the Target Attribute of the Link Tag.
     *
     * @param   string      $target             Use one of the valid attribute values.
     * @throws  InvalidArgumentException
     */
    public function setLinkTarget($target)
    {
        if (is_string($target)) {
            $this->linkTarget = $target;
        } else {
            $msg = t(
                    'Wrong data type submitted. String needed, found "%s".',
                    gettype($target)
            );
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * This Method sets the Link Targets which decide whether activating the Link
     * opens a new Browser Tab or not.
     *
     * @param array $targets
     * @throws InvalidArgumentException
     */
    public function setLinkTargets($targets)
    {
        if (is_array($targets)) {
            $this->linkTargets = $targets;
        } else {
            $msg = t(
                    'The link targets must be an array, found "%s".',
                     gettype($targets)
            );
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * Sets the default Link Text.<br />
     * The link text will be overriden, if a value is found with the defined link-text-handle.<br />
     * The link text is only displayed if <code>$this->hasButton</code> id true.
     *
     * @param   string      $linkText           The default link text of the button. An empty string can also be submitted.<br />
     *                                          To use the default link text, submit null as the method parameter.
     * @throws  InvalidArgumentException
     */
    public function setLinkText($linkText = null)
    {
        if (is_null($linkText)) {
            $this->linkText = $this->defaultLinkText;
        } elseif (is_string($linkText)) {
            $this->linkText = $linkText;
        } else {
            $msg = t(
                    'Wrong data type submitted. String needed, %s found.',
                    gettype($linkText)
            );
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * Overrides the default Link Text Handle.<br />
     * The link-text handle defines the attribute, whichs value does override the button text.
     *
     * @param   string      $linkTextHandle     The new attribute handle.
     * @throws  InvalidArgumentException
     */
    public function setLinkTextHandle($linkTextHandle)
    {
        if (is_string($linkTextHandle)) {
            $this->linkTextHandle = $linkTextHandle;
        } else {
            $msg = t(
                    'Wrong data type submitted. String needed, %s found.',
                    gettype($linkTextHandle)
            );
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * Sets the Link Type.<br />
     * There are three possible values:
     * - <code>self::YELVARO_LINK_TYPE_NONE</code> if no link should be display.
     * - <code>self::YELVARO_LINK_TYPE_SURROND</code> if the link should surround the content. A button can still be added by this object after the content.
     * - <code>self::YELVARO_LINK_TYPE_BUTTON</code> if the link should be a button after the content.
     *
     * @param   integer         $type           Use one of the defined YELVARO_LINK_TYPE - constants of this class.
     * @throws  InvalidArgumentException
     */
    public function setLinkType($type = self::YELVARO_LINK_TYPE_SURROND)
    {
        $validValues = [
            self::YELVARO_LINK_TYPE_NONE,
            self::YELVARO_LINK_TYPE_SURROND,
            self::YELVARO_LINK_TYPE_BUTTON
        ];
        $linkType = intval($type);

        if (in_array($linkType, $validValues)) {
            $this->linkType = $linkType;
        } else {
            $msg = t('The submitted link type "%s" does not exist.', $type);
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * Sets the current Page with the provided Attributes.
     *
     * @param   Page        $page
     * @throws  InvalidArgumentException
     */
    public function setPage($page)
    {
        if (is_object($page)) {
            $this->page = $page;
        } else {
            $msg = t(
                    'Wrong data type submitted. Object needed, %s found.',
                    gettype($page)
            );
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * Sets the Title Attribute of the Link Tag.
     *
     * @param   string      $title
     * @throws  InvalidArgumentException
     */
    public function setTitle($title)
    {
        if (is_string($title)) {
            $this->title = $title;
        } else {
            $msg = t(
                    'Wrong data type submitted. Object needed, %s found.',
                    gettype($title)
            );
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * Sets the Link Target Attribute of the Link Tag.
     *
     * @param   string      $type               "blank" or "new" will set the "_blank" target, "same" or "" will set no target (open the page on the same tab).
     * @throws  InvalidArgumentException
     */
    protected function defineTarget($type)
    {
        if ($type === 'new' || $type === 'blank') {
            $this->setLinkTarget('_blank');
        } elseif ($type === 'same' || $type === '') {
            $this->setLinkTarget('');
        } else {
            $msg = t('Wrong type submitted. "same" or "new" needed.');
            throw new InvalidArgumentException($msg);
        }
    }
    /**
     * Returns the Button Container String.
     *
     * @param string $button                    The content, which needs to be
     *                                          surrounded by the container.
     * @return string
     */
    protected function getButtonContainer($button)
    {
        $tag = $this->getButtonContainerTag();
        if (!empty($tag)) {
            $classes = $this->getButtonContainerClasses();
            $classString = '';
            if (!empty($classes)) {
                $classString = ' class="' . implode(', ', $classes) . '"';
            }
            $return = '<' . $tag . $classString . '>';
            $return .= $button;
            $return .= '</' . $tag . '>';
        } else {
            $return = $button;
        }

        return $return;
    }
    /**
     * Sets the Link Target by the submitted Type.<br />
     * <br />
     * This method will take the value found on the link targets array.
     *
     * @param string $type
     * @throws InvalidArgumentException
     */
    protected function setLinkTargetByType($type)
    {
        $linkTargets = $this->getLinkTargets();
        $linkTargetTypes = array_keys($linkTargets);

        if (!is_string($type)) {
            $msg = t(
                    'The link target type must be a string, found "%s".',
                    gettype($type)
            );
            throw new InvalidArgumentException($msg);
        } if (in_array($type, $linkTargetTypes)) {
            $this->setLinkTarget($linkTargets[$type]);
        } elseif (!empty($linkTargets)) {
            $keys = array_keys($linkTargets);
            $lttString = implode(', ', $keys);
            $msg = t('The link target type must be one of the following '
                    . 'entries: "%1$s", found :"%2$s"', $lttString, $type);
            throw new InvalidArgumentException($msg);
        }
    }
}