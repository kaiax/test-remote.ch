<?php

namespace Application\Utility\Service;

use \BadMethodCallException;
use \InvalidArgumentException;
use \Concrete\Core\Page\Page;
use \Application\Utility\Service\YelvaroNavigationModel;

/**
 * A Helper Class to handle several Navigation Tasks such as:
 * - Back link
 * - Page level
 *
 * A typical use of this class would be as following:
 *
 * <code>
 * $cPage = \Page::getCurrentPage();
 * $nHM = new \Application\Utility\Service\YelvaroNavigationModel([<br />
 *     'currentPage' => $cPage,<br />
 *     'linkTextType' => YelvaroNavigationModel::YELVARO_LINK_TEXT_TYPE_CNAME,<br />
 *     'linkTextWrapper' => '<span>',<br />
 *     'minLevel' => 1<br />
 * ]);<br />
 * if ($isFixedPage) {<br />
 *     $nHM->setFixedPage($cPage);<br />
 * }<br />
 * $nH = new \Application\Utility\Service\Navigation($nHM);<br />
 * $hasBackLink = $nH->getHasLink();
 *
 * // Now display the back link:<br />
 * $nH->getParentLinkTag();<br />
 * </code>
 */

class YelvaroNavigation
{

    /** @var    \Application\Utility\Service\YelvaroNavigationModel */
    private $options;
    /** @var    array */
    private $cTree;

    public function __construct(YelvaroNavigationModel $options = null)
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * Evaluates the Breadcrumb from the current Page to the Page with the ID 1.
     *
     * @param   mixed       $page
     * @throws  InvalidArgumentException
     */
    public function evalParents($page = null)
    {
        if (!is_null($page) && !is_object($page)) {
            $e = t(
                    'Wrong data type submitted. Object or null needed, %s found.',
                    gettype($page)
            );

            throw new InvalidArgumentException($e);
        }

        if (is_null($page)) {
            if (!is_object($this->options)) {
                $this->options = new YelvaroNavigationModel([
                    'currentPage' => Page::getCurrentPage()
                ]);
            }

            if (is_null($this->options->getCurrentPage())) {
                $this->options->setCurrentPage(Page::getCurrentPage());
            }

            $page = $this->options->getCurrentPage();
        }

        $this->cTree = array_reverse($this->getParentRec($page));
    }

    /**
     * Returns the Collection Tree.
     * This tree is a indexed array of the IDs of the parents of the submitted page including its own.
     *
     * @param   object      $page           An existing c5 page.
     * @return  array                       Indexed array of IDs, starting with the ID 1.
     */
    public function getCollectionTree($page = null)
    {
        if (is_null($this->cTree) || is_object($page)) {
            $this->evalParents();
        }

        return $this->cTree;
    }

    /**
     * Returns if it will display a Link according to the specified Parameters.
     *
     * @return  boolean
     */
    public function getHasLink()
    {
        return ! empty($this->options->getFixedPage())
                || $this->getLevel() >= $this->options->getMinLevel();
    }

    /**
     * Returns the Level of the specified Page.<br />
     * The result can be justified by the model parameter <code>$isHomePageCounted</code>
     *
     * @return  int
     * @throws  BadMethodCallException
     */
    public function getLevel()
    {
        if (is_array($this->cTree)) {
            if (is_null($this->options) || $this->options->getIsHomePageCounted()) {
                return count($this->cTree);
            } else {
                return count($this->cTree) - 1;
            }
        } else {
            $e = t('The parent pages are not evaluated yet.');

            throw new BadMethodCallException($e);
        }
    }

    public function getLinkTag($url, $attrStr, $content)
    {
        return '<a href="' . $url . '"' . $attrStr . '>' . $content . '</a>';
    }

    /**
     * @return  \Application\Utility\Service\YelvaroNavigationModel
     */
    public function getModel()
    {
        return $this->options;
    }

    /**
     * @param   object  $page           An existing c5 page.
     * @return  object                  The parent of the specified page as c5 page.
     */
    public function getParent($page = null)
    {
        $cTree = $this->getCollectionTree($page);
        $count = count($cTree);

        if ($count >= 2) {
            return Page::getByID($cTree[$count - 2]);
        } else {
            return null;
        }
    }

    /**
     * Returns a Link Tag if the settings allows it and a Parent Page can be found.
     *
     * @param   mixed       $page               The page whichs parent should be the URL of the link.
     *                                          If null, the page will be taken from the YelvaroNavigationModel.
     * @param   string      $content            To use this parameter, the linkTextType has to be YelvaroNavigationModel::YELVARO_LINK_TEXT_TYPE_CUSTOM.
     * @param   array       $attributes         Link attributes as assoc array.
     * @return  string                          A link tag if a parent page was found with the underlying settings.
     */
    public function getParentLinkTag($page = null, $content = '', $attributes = [])
    {
        $fixedPage = $this->options->getFixedPage();

        if (empty($fixedPage)) {
            $url = $this->getParentUrl($page);
        } else {
            $url = $fixedPage->getCollectionPath();
        }

        $url = $this->setParamsString($url, $this->options);
        $attr = [];

        foreach ($attributes as $key => $value) {
            $attr[] = ' ' . $key . '="' . $value . '"';
        }

        $attrStr = implode(',', $attr);

        if (!empty($fixedPage) || $this->getLevel() >= $this->options->getMinLevel()) {
            $linkTextType = $this->options->getLinkTextType();

            if ($linkTextType === YelvaroNavigationModel::YELVARO_LINK_TEXT_TYPE_CNAME) {
                if (empty($fixedPage)) {
                    $content = $this->getParent()->getCollectionName();
                } else {
                    $content = $fixedPage->getCollectionName();
                }
            } elseif ($linkTextType === YelvaroNavigationModel::YELVARO_LINK_TEXT_TYPE_CUSTOM) {
                $content = $this->options->getLinkText();
            }

            $contentWrapper = $this->options->getLinkTextWrapper();

            if (!empty($contentWrapper)) {
                $wrapperElement = str_replace(['<', '>'], '', $contentWrapper);
                $content = '<' . $wrapperElement . '>' . $content . '</' . $wrapperElement . '>';
            }

            return $this->getLinkTag($url, $attrStr, $content);
        } else {
            return '';
        }
    }

    /**
     * Returns the URL of the Parent of the submitted Page.
     *
     * @param   mixed       $page           The page whichs parent URL is needed.
     * @return  string                      The relative path of the parent, or an empty string if no parent page was found.
     */
    public function getParentUrl($page = null)
    {
        $parent = $this->getParent($page);

        return is_null($parent)
                ? ''
                : ($parent->getCollectionID() == 1 ? '/' : $parent->getCollectionPath());
    }

    /**
     * Sets the option and evaluates the Parents if the corresponding Option is set.
     *
     * @param   YelvaroNavigationModel $options
     */
    public function setOptions(YelvaroNavigationModel $options)
    {
        $this->options = $options;

        if ($options->getEvalParents()) {
            $this->evalParents($options->getCurrentPage());
        }
    }

    /**
     * Builds an Array with the Breadcrumb of the submitted Page.
     *
     * @param   object      $page           The concrete5 page, from which the breadcrumb should be evaluated.
     * @param   array       $arr            The resulting array needed by this method.
     * @return  array                       An indexed array with the current page as the first entry and the root as the last item.
     *
     * @todo    The cID = 1 does not work with a multilingual enviroment!!!!!
     */
    protected function getParentRec($page, $arr = [])
    {
        $cID = intval($page->getCollectionID());

        if ($cID === 0) {
            return $arr;
        } else {
            $arr[] = $cID;

            if ($cID === 1) {
                return $arr;
            } else {
                return $this->getParentRec(
                        Page::getByID($page->getCollectionParentID()),
                        $arr
                );
            }
        }
    }

    /**
     * Adds GET params to the submitted URL if params are defined. Otherwise the submitted URL will be returned.<br />
     * To use this method please use one of the following ways:
     * - <code>$nmH->setSetParamsFromRequest($boolean)</code> - Use the boolean method from the navigation model object, to obtain the params from the request.
     * - <code>$nmH->setParamsString(string)</code> - Use the string method from the navigation model object to set the params directly from the submitted string.
     *
     * @param   string                                          $url        The current page path.
     * @param   Application\Utility\Service\YelvaroNavigationModel     $nmH        The current navigation model object.
     * @return  string                                                      The expanded URL, or if no params can be found the submitted URL.
     */
    protected function setParamsString($url, YelvaroNavigationModel $nmH)
    {
        if ($nmH->getSetParamsFromRequest()) {
            if (! empty($_REQUEST)) {
                $params = '';
                foreach ($_REQUEST as $key => $value) {
                    $params .= $key . '=' . urlencode($value) . '&';
                }
                return $url . '?' . substr($params, 0, -1);
            } else {
                return $url;
            }
        } else {
            $paramsString = $nmH->getParamsString();

            if (empty($paramsString)) {
                return $url;
            } else {
                return $url . (strpos($paramsString, '?') === 0 ? '' : '?') . $paramsString;
            }
        }
    }

}
