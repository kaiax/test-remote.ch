<?php
namespace Application\Utility\Service;

use \BadMethodCallException;
use \Error;
use \Exception;
use \InvalidArgumentException;
use Config;
use Core;
use \Concrete\Core\File\Image\Thumbnail\Type\Type as ThumbnailType;
use \Concrete\Core\Localization\Localization;
use \Concrete\Core\Multilingual\Page\Section\Section as MultilingualSection;
use \Concrete\Core\Page\Page;
use \Concrete\Core\Page\Theme\Theme;
use \Concrete\Core\Site\SiteList;
use \Concrete\Core\Support\Facade\Application;
use \Concrete\Core\View\View;
use \Application\Utility\Service\YelvaroLink;
use \Application\Utility\Service\YelvaroMultilingual;

/**
 * A Helper Class to handle several Image Tasks such as:
 * - Image tag string
 * - backstretch implementation (the js implementation of the backstretch code still needs to be done)
 *
 * The images can be searched from the sitetree by an attribute handle, or by
 * the image name from the corresponding theme folder.<br />
 * CSS classes can be added by <code>addClassImage()</code>, the alt attribute
 * can be set by <code>setAlt()</code>
 *
 * If the URL set by <code>setLinkUrl()</code> is not an existing page, the
 * <code>setIsLinkExact()</code> needs to be set to true.
 *
 * A typical use of this class would be as following:
 *
 * <code>
 * $miH = new \Application\Utility\Service\YelvaroImage();<br />
 * $miH->setAlt('Denkfabrik GmbH'); // set the alt attribute of the image tag<br />
 * $miH->resetClass('img'); // remove the default class' "img img-responsive"<br />
 * $miH->addClass('img', 'my-class');<br />
 * $miH->addClass('a', 'my-link-class');<br />
 * $miH->setIsMultilingual();<br />
 * $miH->setLinkUrl('/');<br />
 * echo $miH->getImageTagByPageAttribute('header_image');
 * </code>
 *
 * @todo: in <code>getImageTagWithLink()</code> hats einen Aufruf auf die Methode
 * <code>getMultilingualPageByUrl()</code>, welche Multilingual spezifische
 * Abfragen macht. -> Dies ist nicht der korrekte Ort! Entweder in der YelvaroLink
 * oder in der YelvaroMultilingual Class behandeln!!
 */
class YelvaroImage
{
    /** @var string */
    private $alt = '';

    /** @var array */
    private $class = [
        'a' => null,
        'img' => 'img img-responsive'
    ];

    /** @var boolean */
    private $hasLink = false;

    /** @var boolean */
    private $isLinkExact = false;

    /** @var boolean */
    private $isMultilingual = false;

    /** @var string */
    private $linkUrl = '';

    /** @var \Application\Utility\Service\YelvaroLink */
    private $mlH = '';

    /** @var \Application\Utility\Service\YelvaroMultilingual */
    private $mlsH = '';

    /** @var array */
    private $themeFolders = [
        'img' => 'images'
    ];

    /**
     * @var ThumbnailType
     */
    private $thumbnailType;

    public function __construct($createYelvaroLink = false, $isMultilingual = null)
    {
        $this->app = Application::getFacadeApplication();

        if (is_object($createYelvaroLink)) {
            $this->setYelvaroLinkHelper($createYelvaroLink);
        } elseif ($createYelvaroLink === true) {
            $this->createYelvaroLinkHelper();
        }

        if (is_bool($isMultilingual)) {
            $this->setIsMultilingual($isMultilingual);
        } else {
            $this->setIsMultilingual($this->getApp()->make('multilingual/detector')->isEnabled());
        }

        if ($this->getIsMultilingual()) {
            $this->createYelvaroMultilingualHelper();
        }
    }

    /**
     * Adds a Class to the specified Type.<br />
     *
     * Following types are supported:
     * - "a" for hyperlinks
     * - "img" for image tags
     *
     * By adding several classes with space between, multiple classes can be added by one call.
     *
     * @param   string      $type
     * @param   string      $class
     *
     * @throws InvalidArgumentException
     */
    public function addClass($type, $class)
    {
        if (!is_string($class) || !is_string($type)) {
            $typeType = gettype($type);
            $classType = gettype($class);

            if ($typeType !== 'string') {
                $arr[] = $typeType;
            }
            if ($classType !== 'string') {
                $arr[] = $classType;
            }

            $str = implode('/', $arr);
            $msg = t(
                    'Wrong data type submitted. String needed, %s found.',
                    $str
            );
            throw new InvalidArgumentException($msg);
        } elseif (!in_array($type, array_keys($this->class))) {
            $msg = 'Wrong tag type found: ' . $type;

            throw new InvalidArgumentException($msg);
        } else {
            $classArr = explode(' ', $this->class[$type]);
            $classArr[] = $class;

            if (isset($classArr[0]) && empty($classArr[0])) {
                unset($classArr[0]);
            }

            $this->class[$type] = implode(' ', $classArr);
        }
    }

    /**
     * Returns the value of the alt Variable of the Image.
     * @return  string
     */
    public function getAlt()
    {
        return $this->alt;
    }

    /**
     * Returns the Alt Text as an Attribute String.<br />
     * e.g. <code>' alt="alt text"'</code>
     *
     * @param   boolean     $returnAnyway           If false, the alt attribute string will be empty if the alt value is empty.
     * @param   boolean     $addStartingSpace       If true, an additional space will be added before the alt attribute.
     * @return  string
     */
    public function getAltAttribute($returnAnyway = true, $addStartingSpace = true)
    {
        if ($returnAnyway || $this->getAlt()) {
            return ($addStartingSpace ? ' ' : '') . 'alt="' . $this->getAlt() . '"';
        }
    }

    /**
     * Returns the c5 Application Object.
     *
     * @return  \Concrete\Core\Support\Facade\Application
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Returns the Script String of the Backstretch Implementation.<br />
     * If this method is used, the backstretch jQuery Plugin needs to be implemented.
     *
     * @param   mixed       $files                  Contains the file(s) path. The alt attribute of the image can also be defined. Submit this parameter as string or array.
     * @param   string      $selector               The selector, where the backstretch should be binded to. If empty, the backstretch will be added to the body.
     * @param   mixed       $options                The provided backstretch options.
     * @return  string
     */
    public function getBackstretch($files, $selector, $options)
    {
        if (!empty($files)) {
            if (!is_string($files)) {
                $files = json_encode($files, JSON_UNESCAPED_SLASHES);
            }

            $bkst = empty($selector) ? '$.' : '$("' . $selector . '").';

            if (!empty($options)) {
                if (is_string($options)) {
                    $optionsStr = ', ' . $options;
                } else {
                    $optionsStr = ', ' . json_encode($options);
                }
            } else {
                $optionsStr = '';
            }

            $script = '<script>';
                $script .= '$(function() {';
                    $script .= $bkst . 'backstretch(' . $files . $optionsStr . ');';
                $script .= '});';
            $script .= '</script>';

            return $script;
        } else {
            return '';
        }
    }

    /**
     * Returns the Script String of the Backstretch Implementation.<br />
     * If this method is used, the backstretch jQuery Plugin needs to be implemented.<br />
     * The possible options can be found on: {@see https://github.com/jquery-backstretch/jquery-backstretch}<br />
     *
     * This method searches for the submitted attribute. If the current page has not defined the attribute,<br />
     * the ancestors are search for.
     *
     * @param   string      $attributeHandle        The attribute handle which should be found. If no page was found, en empty string will be returned.
     * @param   Page        $c                      The current Page which should display the backstretch.
     * @param   string      $selector               The selector, where the backstretch should be binded. If empty, the backstretch would be added to the body.
     * @param   mixed       $options                The backstretch options as string or array.
     * @return  string
     */
    public function getBackstretchByPageAttribute($attributeHandle, $c, $selector, $options)
    {
        if (!is_object($c)) {
            $c = Page::getCurrentPage();
        }
        $page = $this->getPageWithAttribute($attributeHandle, $c);

        if (is_object($page)) {
            $fv = $page->getAttribute($attributeHandle)->getApprovedVersion();

            $alt = $this->getAlt();
            if (empty($alt)) {
                $fvDesc = $fv->getDescription();
                $alt = empty($fvDesc) ? $fv->getTitle() : $fvDesc;
            }

            $src = $fv->getRelativePath();
            $str = '["' . $src . '"]';
//            $str = '[{"url": "' . $src . '", "alt": "' . $alt . '"}]';

            return $this->getBackstretch($str, $selector, $options);
        } else {
            return '';
        }
    }

    /**
     * Returns the Class Attribute String for an HTML Element.
     *
     * @param   string      $type
     * @param   string      $addStartingSpace
     * @return  string
     */
    public function getClassAttribute($type, $addStartingSpace = true)
    {
        if (isset($this->class[$type])) {
            if (!empty($this->class[$type])) {
                return ($addStartingSpace ? ' ' : '') . 'class="' . $this->class[$type] . '"';
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    /**
     * Get the css definition of a defined image.
     *
     * This method will return the background-image CSS definition with the
     * url of the found image or an empty string, if no image could be found.
     * The resulting string will be of kind:
     * <code>background-image: url(your-url.file-extension)</code>.
     * This method will internally call <code>$this->getImageByPageAttribute()</code>
     * to achieve the result.
     *
     * @param   string      $atHandle       The attribute handle of the image
     *                          to find.
     * @param   mixed       $c              The base page to get the desired image.
     *                          If its not defined, the current page will be taken.
     * @param   boolean     $searchPage     If true and the attribute is not set
     *                          on the current page, the first ancestor with this
     *                          attribute will be taken instead.
     * @param   boolean     $doAddStyle     If true the returned will be surrounded
     *                          by <code> style="..."</code>.
     * @return string
     */
    public function getCssBgImageByPageAttribute(
            $atHandle,
            $c = null,
            $searchPage = true,
            $doAddStyle = true
    )
    {
        $str = '';
        $fv = $this->getImageByPageAttribute($atHandle, $c, $searchPage);

        if (is_object($fv)) {
            $str = 'background-image: url(' . $fv->getRelativePath() . ')';
        }

        if ($doAddStyle && ! empty($str)) {
            $str = ' style="' . $str . '"';
        }

        return $str;
    }

    /**
     * Returns the default Locale of the current Site, resp. the first Site found.
     *
     * @return  string
     */
    public function getDefaultLocale()
    {
        $defaultLocale = Config::get('concrete.multilingual.default_locale');
        if (!$defaultLocale) {
            if (class_exists('\Concrete\Core\Site\SiteList')) {
                $sl = new SiteList();
                $sites = $sl->get();
                foreach ($sites as $site) {
                    $defaultLocale = $site->getDefaultLocale();
                    break;
                }
            }
        }
        return $defaultLocale;
    }

    /**
     * Returns the Approved Version of a File if a page could be found in the Page Tree with the requested Attribute.<br />
     * The search begins on the submitted page and goes down the tree. The page with the <code>cID = 1</code><br />
     * is the last page to check. System pages will not be checked.<br />
     * The img classes and the alt attribute can be set by the corresponding methods of this class.
     *
     * @param   string      $atHandle               The attribute handle of the image to display.
     * @param   Page        $c                      The page with the desired attribute, or the page whose parents should be checked for the submitted attribute.
     * @param   boolean     $searchPage             If true and the attribute is not set on the current page, the first parent with this attribute will be taken instead.
     * @return  Concrete\Core\File\Version|null
     */
    public function getImageByPageAttribute($atHandle, $c = null, $searchPage = true)
    {
        if (! is_object($c)) {
            $c = Page::getCurrentPage();
        }

        if (is_object($c)) {
            if ($searchPage) {
                $page = $this->getPageWithAttribute($atHandle, $c);
            } else {
                $page = $c->getAttribute($atHandle) ? $c : null;
            }
        }

        if ($page !== null) {
            $img = $page->getAttribute($atHandle);

            if (is_object($img)) {
                return $img->getApprovedVersion();
            }
        }

        return null;
    }

    /**
     * Returns an Image Tag as string with the Path found on the submitted Page with the specified Attribute.
     *
     * @param   string      $src                    The path to the image which should be displyed.
     * @param   array       $fAttributes            An assoc array with the attribute name as key and the attribute value as value.<br />
     *                                              To set the alt attribute of the image, use the <code>setAlt()</code> of this class.
     * @return  string
     */
    public function getImageTag($src, $fAttributes = [])
    {
        if (key_exists('alt', $fAttributes)) {
            $this->setAlt($fAttributes['alt']);
            unset($fAttributes['alt']);
        }

        $attributeString = '';
        foreach ($fAttributes as $key => $value) {
            $attributeString .= ' ' . $key . '="' . $value . '"';
        }

        return '<img src="'
                    . $src . '"'
                    . $this->getAltAttribute()
                    . $this->getClassAttribute('img')
                    . $attributeString
                . ' />';
    }

    /**
     * Returns an Image Tag with an Image of the Theme Folder.<br />
     * To add a surrounding link tag, use the <code>setLinkUrl()</code> of this class.<br />
     * The multilingual link URL is also supported.
     *
     * @param   string      $fileName
     * @param   array       $fAttributes            An assoc array with the attribute name as key and the attribute value as value.
     * @return  string
     */
    public function getImageTagFromThemeFolder($fileName, $fAttributes = [])
    {
        $themePath = View::getInstance()->getThemePath();
        if (!$themePath) {
            // coming from an AJAX call, the View does not know the theme path
            $theme = Theme::getSiteTheme();
            $themePath = $theme->getThemeURL();
        }

        $src = $themePath . '/' . $this->getThemeFolder('img') . '/' . $fileName;
        if ($this->hasLink) {
            return $this->getImageTagWithLink($src, $fAttributes);
        } else {
            return $this->getImageTag($src, $fAttributes);
        }
    }

    /**
     * Returns an Image Tag if a page could be found in the Page Tree with the requested Attribute.<br />
     * The search begins on the submitted page and goes down the tree. The page with the <code>cID = 1</code><br />
     * is the last page to check. System pages will not be checked.<br />
     * The img classes and the alt attribute can be set by the corresponding methods of this class.
     *
     * @param   string      $atHandle               The attribute handle of the image to display.
     * @param   Page        $c                      The page with the desired attribute, or the page whose parents should be checked for the submitted attribute.
     * @param   boolean     $searchPage             If true and the attribute is not set on the current page, the first parent with this attribute will be taken instead.
     * @return  string
     */
    public function getImageTagByPageAttribute($atHandle, $c = null, $searchPage = true)
    {
        $img = $this->getImageByPageAttribute($atHandle, $c, $searchPage);

        if (is_null($img)) {
            return '';
        } else {
            if ($this->thumbnailType === null) {
                $path = $img->getRelativePath();
            } else {
                $path = $img->getThumbnailURL($this->thumbnailType->getBaseVersion());
            }

            if ($this->hasLink) {
                return $this->getImageTagWithLink($path);
            } else {
                return $this->getImageTag($path);
            }
        }
    }

    /**
     * Returns a linked Image Tag.<br />
     *
     * To use this method, the <code>$linkUrl</code> has to be set by <code>setLinkUrl()</code>.
     *
     * @param   string      $src
     * @param   array       $fAttributes            An assoc array with the attribute name as key and the attribute value as value.
     * @return  string
     */
    public function getImageTagWithLink($src, $fAttributes)
    {
        $str = '';

        if ($this->isLinkExact) {
            $cPath = $this->linkUrl;
        } else {
            if ($this->isMultilingual) {
                $c = $this->getMultilingualPageByUrl($this->linkUrl);
            } else {
                $c = Page::getByPath($this->linkUrl);
            }

            $cPath = $c->getCollectionPath();
            if (empty($cPath)) {
                $cPath = '/';
            }
        }

        $str .= '<a href="' . $cPath . '"' . $this->getClassAttribute('a') . '>';
            $str .= $this->getImageTag($src, $fAttributes);
        $str .= '</a>';

        return $str;
    }

    public function getIsMultilingual()
    {
        return $this->isMultilngual;
    }

    /**
     * Returns the YelvaroLink Instance. If it was not already created, a new one will be created.
     *
     * @return  \Application\Utility\Service\YelvaroLink
     */
    public function getYelvaroLinkHelper()
    {
        if (is_null($this->mlH)) {
            $this->createYelvaroLinkHelper();
        }

        return $this->mlH;
    }

    /**
     * Returns the YelvaroMultilingual Instance. If it was not already created, a new one will be created.
     *
     * @return  \Application\Utility\Service\YelvaroMultilingual
     */
    public function getYelvaroMultilingualHelper()
    {
        if (is_null($this->mlsH)) {
            $this->createYelvaroMultilingualHelper();
        }

        return $this->mlsH;
    }

    /**
     * Returns the Page with the submitted URL related to the active Locale.
     *
     * @param   string      $url
     * @return  c5-Page
     *
     * @throws  InvalidArgumentException
     */
    public function getMultilingualPageByUrl($url)
    {
        if (!is_string($url) && !is_object($url)) {
            $e = t(
                    'Wrong data type submitted. String or c5-page object needed, %s found.',
                    gettype($url)
            );

            throw new InvalidArgumentException($e);
        }

        if (empty($this->mlsH)) {
            $this->createYelvaroMultilingualHelper();
        }

        if (empty($url)) {
            $url = '/';
        }

        $this->mlsH->setDefaultUrl($url);
        return $this->mlsH->getTranslatedUrl();

        // $activeLocale = Localization::activeLocale();
        // $c = $url;

        // if (is_string($url)) {
            // if ($url === '/') {
                // Atm with c5-8.2.1 the root page are not connected if the default locale does not have an page with a path "/{$language}"
                // $c = Page::getByID(1);
                // $defaultLocale = $this->getDefaultLocale();
                // if ($defaultLocale !== $activeLocale) {
                    // $c = Page::getByPath('/' . Localization::activeLanguage());
                // }
                // return $c;
            // } else {
                // $c = Page::getByPath($url);
            // }
        // }

        // if (!is_object($c)) {
            // throw new InvalidArgumentException(t('The page with the path %s does not exist.', $url));
        // } elseif ($c->isSystemPage()) {
            // throw new InvalidArgumentException(t('The page with the URL %s is a system page. Therfore no multilingual page relation can be found'));
        // }

        // $mprID = MultilingualSection::getMultilingualPageRelationID($c->getCollectionID());
        // if (!empty($mprID)) {
            // return Page::getByID(MultilingualSection::getCollectionIDForLocale($mprID, $activeLocale));
        // } else {
            // return $c;
        // }
    }

    /**
     * Returns the nearest Page from the Tree which has the requested Attribute.
     *
     * @param   string      $attributeHandle
     * @param   Page        $c
     * @return  Page
     *
     * @throws  InvalidArgumentException
     */
    public function getPageWithAttribute($attributeHandle, Page $c)
    {
        if (empty($attributeHandle)) {
            $msg = t('Empty string for the attribut name submitted.');
            throw new InvalidArgumentException($msg);
        } elseif (!is_string($attributeHandle)) {
            $msg = t(
                    'Wrong data type submitted. String needed, %s found.',
                    gettype($attributeHandle)
            );
            throw new InvalidArgumentException($msg);
        } elseif ($c->isSystemPage()) {
            $c = Page::getByID(1);
        }

        return $this->recPageWithAttribute($attributeHandle, $c);
    }

    /**
     * Returns the current Folder specified by $type.
     *
     * @param   string      $type
     * @return  string
     */
    public function getThemeFolder($type)
    {
        return $this->themeFolders[$type];
    }

    /**
     * Reset the current instance to the default values.
     */
    public function reset()
    {
        $this->alt = '';
        $this->hasLink = false;
        $this->isLinkExact = false;
        $this->thumbnailType = null;
    }

    /**
     * Empties the Class' of the specified $type.
     *
     * @param   string      $type
     */
    public function resetClass($type)
    {
        $this->class[$type] = '';
    }

    /**
     * Sets the Alt Attribute used by the img Tags.
     *
     * @param   string      $alt
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    }

    /**
     * If set to true, the Image tag will be surrounded by a Link Tag.<br />
     * Therfore a link has to be set.
     *
     * @param   boolean     $hasLink
     *
     * @throws  InvalidArgumentException
     */
    public function setHasLink($hasLink = true)
    {
        if (!is_bool($hasLink)) {
            $msg = t(
                    'Wrong data type submitted. Boolean needed, %s found.',
                    gettype($hasLink)
            );
            throw new InvalidArgumentException($msg);
        }
        $this->hasLink = $hasLink;
    }

    /**
     * Sets the isLinkExct Variable which controls the kind of the Link.<br />
     * If this variable is set to true, the link will not be checked against an existing page.<br />
     * Otherwise the link has to be an existing page.
     *
     * @param   boolean     $isLinkExact
     */
    public function setIsLinkExact($isLinkExact = true)
    {
        $this->isLinkExact = $isLinkExact ? true : false;
    }

    /**
     * Sets the URL which will be used by <code>getImageTagByPageAttribute()</code>, <code>getImageTagFromThemeFolder()</code> and <code>getImageTag()</code>.<br />
     * This method will also set the $hasLink to true. To reset the <code>$linkURL</code> use this method with an empty string.
     *
     * @param   mixed       $url                c5-page object or string
     *
     * @throws  InvalidArgumentException
     */
    public function setLinkUrl($url)
    {
        if (!is_string($url) && !is_object($url)) {
            $msg = t(
                    'Wrong data type submitted. String or c5-page object '
                    . 'needed, %s found.',
                    gettype($url)
            );
            throw new InvalidArgumentException($msg);
        }

        $this->linkUrl = is_string($url) ? $url : $url->getCollectionPath();

        if (empty($url)) {
            $this->setHasLink(false);
        } else {
            $this->setHasLink();
        }
    }

    /**
     * If set to true, the URL for the Link will be transformed to the corresponding Page of the current Locale if needed.
     *
     * Set this parameter only to true, if the following requirements are meet:
     * - The site provides multilingual enviroment. Otherwise a <code>BadMethodCallException</code> will be thrown.
     * - The <code>$linkUrl</code> (set with <code>setLinkUrl()</code>) exists on all other languages. Otherwise the site root will be defined for the missing relation(s).
     * - The <code>$linkUrl</code> is mapped on the site report dashoard page. Otherwise the site root will be defined for the missing relation(s).
     *
     * @param   boolean     $isMultilingual
     *
     * @throws  BadMethodCallException
     * @throws  InvalidArgumentException
     */
    public function setIsMultilingual($isMultilingual = true)
    {
        if (!is_bool($isMultilingual)) {
            $msg = t(
                    'Wrong data type submitted. Boolean needed, %s found.',
                    gettype($isMultilingual)
            );
            throw new InvalidArgumentException($msg);
        // } elseif (!Core::make('multilingual/detector')->isEnabled()) {
            // $msg = t('This site has no multilingual enviroment.');
            // throw new BadMethodCallException($msg);
        }
        $this->isMultilingual = $isMultilingual;
    }

    /**
     * Sets the YelvaroLink Instance.
     *
     * @param   YelvaroLink   $mlH
     * @throws  InvalidArgumentException
     */
    public function setYelvaroLinkHelper(YelvaroLink $mlH)
    {
        $this->mlH = $mlH;
    }

    /**
     * Sets the YelvaroMultilingual Instance.
     *
     * @param   YelvaroMultilingual   $mlsH
     * @throws  InvalidArgumentException
     */
    public function setYelvaroMultilingualHelper(YelvaroMultilingual $mlsH)
    {
        $this->mlsH = $mlsH;
    }

    /**
     * Sets the current Theme Folder of the $type.
     *
     * Supported types are:
     * - img
     *
     * @param   string      $type
     * @param   string      $name
     * @throws InvalidArgumentException
     */
    public function setThemeFolder($type, $name)
    {
        if (!in_array($type, array_keys($this->themeFolders))) {
            $typesString = implode(' ', array_keys($this->themeFolders));
            $msg = t(
                    'Unknown type found: %s. Following types are supported: %s.',
                    $type,
                    $typesString
            );
            throw new InvalidArgumentException($msg);
        } elseif (!is_string($name)) {
            $msg = t(
                    'Wrong data type submitted. String needed, %s found.',
                    gettype($name)
            );
            throw new InvalidArgumentException($msg);
        } else {
            $this->themeFolders[$type] = $name;
        }
    }

    /**
     * Set the thumbnail type handle.
     *
     * This method will internally set the thumbnail type property by the present
     * thumbnail type handle.
     * If the thumbnail type was set and
     * <code>$this->getImageTagByPageAttribute()</code> is called, the resulting
     * url will be the one of the requested image and the specified thumbnail type.
     * After setting the thumbnail type, calling <code>$this->reset()</code> will
     * undo the thumbnail type specification.
     *
     * @param   string      $handle
     * @throws  InvalidArgumentException
     */
    public function setThumbnailTypeHandle($handle)
    {
        if (! is_string($handle)) {
            $e = sprintf(
                    'Wrong datatype! Found %1$s, need %2$s.',
                    gettype($handle),
                    'string'
            );

            throw new InvalidArgumentException($e);
        }

        $this->thumbnailType = ThumbnailType::getByHandle($handle);
    }

    /**
     * Creates a YelvaroLink Object and throws an Exception if the Class could not be found.
     *
     * @throws  BadMethodCallException
     */
    protected function createYelvaroLinkHelper()
    {
        try {
            $mlH = new YelvaroLink();
        } catch (Error $err) {
            throw new BadMethodCallException($err->getMessage());
        } catch (Exception $ex) {
            throw new BadMethodCallException($ex->getMessage());
        }
        $this->setYelvaroLinkHelper($mlH);
    }

    /**
     * Creates a YelvaroMultilingual Object and throws an Exception if the Class could not be found.
     *
     * @throws  BadMethodCallException
     */
    protected function createYelvaroMultilingualHelper()
    {
        try {
            $mlsH = new YelvaroMultilingual();
        } catch (Error $err) {
            throw new BadMethodCallException($err->getMessage());
        } catch (Exception $ex) {
            throw new BadMethodCallException($ex->getMessage());
        }
        $this->setYelvaroMultilingualHelper($mlsH);
    }

    /**
     * Searches recursively for a Page with a Value of the submitted Attribute Handle.<br />
     * It begins on the specified Page and walks the Tree down.
     *
     * @param   string      $attributeHandle
     * @param   Page        $c
     * @return  Page
     */
    protected function recPageWithAttribute($attributeHandle, Page $c)
    {
        if (!empty($c->getAttribute($attributeHandle))) {
            return $c;
        } elseif ($c->getCollectionID() == 1 || $c->getCollectionParentID() == 0) {
            return null;
        } else {
            $cParent = Page::getByID($c->getCollectionParentID());
            if (!is_object($cParent)) {
                return null;
            }
            return $this->recPageWithAttribute($attributeHandle, $cParent);
        }
    }

}
