<?php
namespace Application\Utility\Service;

use \BadMethodCallException;
use \InvalidArgumentException;
use \Concrete\Core\Multilingual\Page\Section\Section as MultilingualSection;
use \Concrete\Core\Support\Facade\Application;
use Page;

class YelvaroMultilingual
{
    /**
     * Use this translateType to gain all Informations from the Current Page.<br />
     * This is the only implemented type atm.
     */
    const YELVARO_MULTILINGUAL_CURRENT_PAGE       = 1;
    /**
     * Not implemented atm.
     */
    const YELVARO_MULTILINGUAL_SUBMITTED_LANG     = 2;
    /**
     * Not implemented atm.
     */
    const YELVARO_MULTILINGUAL_SUBMITTED_LOCALE   = 3;

    protected $app;

    /**
     * The Current Page as a concrete5 Page.
     * @var     Page
     */
    private $currentPage;
    /**
     * The URL to Translate into a Multilingual Path.
     * @var     string
     */
    private $defaultUrl;
    /**
     * This is a Flag to Prevent the defined URL from Translating into a
     * Multilingual Path
     * @var     boolean
     */
    private $doNotTranslateUrl = false;
    /**
     * Defines one of the YELVARO_MULTILINGUAL_YELVARO Constants.
     * @var     integer
     */
    private $translateType;

    /**
     * Instanciates a YelvaroMultilingual Object.
     *
     * @param   mixed       $defaultUrl         The URL to Translate or a
     *                                          c5-Page to get the URL from.
     * @param   integer     $translateType      The translation type to be
     *                                          applied to. Use one of the
     *                                          <code>YELVARO_MULTILINGUAL_YELVARO</code>
     *                                          Constants.
     */
    public function __construct(
            $defaultUrl = '',
            $translateType = self::YELVARO_MULTILINGUAL_CURRENT_PAGE
    )
    {
        $this->app = Application::getFacadeApplication();

        if (!empty($defaultUrl)) {
            if (is_string($defaultUrl)) {
                $this->setDefaultUrl($defaultUrl);
            } else {
                $this->setDefaultUrlByPage($defaultUrl);
            }
        }
        $this->setTranslateType($translateType);
    }
    /**
     * Returns the c5 Application Object.
     *
     * @return  \Concrete\Core\Support\Facade\Application
     */
    public function getApp()
    {
        return $this->app;
    }
    /**
     * Returns the Current Page.<br />
     * If the current Page was not already set, it will be grabbed first.
     *
     * @return  Page
     */
    public function getCurrentPage()
    {
        if (is_null($this->currentPage)) {
            $this->setCurrentPage(Page::getCurrentPage());
        }
        return $this->currentPage;
    }
    /**
     * Returns the Default URL set with the Constructor or overriden later.
     *
     * @return  string                  The URL to be translated.
     */
    public function getDefaultUrl()
    {
        return $this->defaultUrl;
    }
    /**
     * Returns whetever the the Default URL should be translated or not.
     *
     * @return boolean
     */
    public function getDoNotTranslateUrl()
    {
        return $this->doNotTranslateUrl;
    }
    /**
     * Returns the Translate Type to be used on the Default URL.
     *
     * @return  integer                 The value of one of the
     *                                  YELVARO_MULTILINGUAL_YELVARO Constants.
     */
    public function getTranslateType()
    {
        return $this->translateType;
    }
    /**
     * Returns the Translated URL, by respecting the submitted Settings.<br />
     * If the default URL was not defined before, an exception will be thrown.
     *
     * @return  string                  The translated URL.
     * @throws  BadMethodCallException
     */
    public function getTranslatedUrl()
    {
        $defaultUrl = $this->getDefaultUrl();
        if (empty($defaultUrl)) {
            $msg = t(
                'The defaultUrl is empty. Please submit this URL before '
                    . 'calling this method.'
            );
            throw new BadMethodCallException($msg);
        }

        if ($this->getDoNotTranslateUrl()) {
            return $defaultUrl;
        } elseif (!$this->getApp()->make('multilingual/detector')->isEnabled()) {
            return $defaultUrl;
        } else {
            $translatedUrl = $defaultUrl;
            $translateType = $this->getTranslateType();
            if ($translateType === self::YELVARO_MULTILINGUAL_CURRENT_PAGE) {
                $currentSection = MultilingualSection::getCurrentSection();

                if ($defaultUrl === '/') {
                    $defaultPage = Page::getByID(1);
                } else {
                    $defaultPage = Page::getByPath($defaultUrl);
                }

                $defaultPageID = $defaultPage->getCollectionID();

                if (MultilingualSection::isMultilingualSection($defaultPageID)) {
                    $translatedPage = $currentSection;
                } else {
                    $mprID = MultilingualSection::getMultilingualPageRelationID($defaultPageID);
                    if (!empty($mprID)) {
                        $activeLocale = $currentSection->getLocale();
                        $cIdTranslated = MultilingualSection::getCollectionIDForLocale($mprID, $activeLocale);
                        $translatedPage = Page::getByID($cIdTranslated);
                    } else {
                        $msg = t('There could not be found a multilingual page relation of the page with the cID: %s.', $defaultPageID);
                        throw new BadMethodCallException($msg);
                    }
                }
                if (is_null($translatedPage)) {
                    // e.g. /login is not part of a multilingual section.
                    // set the default URL.
                    $translatedPage = $defaultPage;
                }
                $translatedUrl = $translatedPage->getCollectionPath();

                if (empty($translatedUrl)) {
                    $translatedUrl = '/';
                }
            } elseif ($translateType === self::YELVARO_MULTILINGUAL_SUBMITTED_LANG
                    || $translateType === self::YELVARO_MULTILINGUAL_SUBMITTED_LOCALE) {
                $msg = 'The translateType ' . $translateType . ' is not supported atm.';
                throw new BadMethodCallException($msg);
            }
            return $translatedUrl;
        }
    }
    /**
     * Sets the Current Page.
     *
     * @param   Page        $c              A c5-page as the current page.
     */
    public function setCurrentPage(Page $c)
    {
        $this->currentPage = $c;
    }
    /**
     * Sets the Default URL after some checks.<br />
     * If one of the checks fails, an exception will be thrown.<br />
     * The default URL will be the path, which will be translated into the
     * current locale.
     *
     * @param   string      $defaultUrl
     * @throws  InvalidArgumentException
     * @throws  BadMethodCallException
     */
    public function setDefaultUrl($defaultUrl)
    {
        if (!is_string($defaultUrl)) {
            $msg = t(
                'The submitted type needs to be "string", found "%s".',
                gettype($defaultUrl)
            );
            throw new InvalidArgumentException($msg);
        }
        $url = trim($defaultUrl);
        if (empty($url)) {
            $msg = t('The submitted path is empty.');
            throw new BadMethodCallException($msg);
        }

        // additionally check, if a page with the submitted path exists?

        $this->defaultUrl = $url;
    }
    /**
     * Sets the Default URL from the submitted Page by calling
     * <code>$this->setDefaultUrl($defaultUrl)</code>.
     *
     * @param   Page        $c              The page to retrieve the path to
     *                                      be set as the default URL.
     */
    public function setDefaultUrlByPage(Page $c)
    {
        $cPath = $c->getCollectionPath();
        if (empty($cPath)) {
            $cPath = '/';
        }

        $this->setDefaultUrl($cPath);
    }
    /**
     * Sets the Flag to avoid the Translation of the Default URL.
     *
     * @param   boolean     $doNotTranslateUrl  True if the default URL should
     *                                          not be translated.
     */
    public function setDoNotTranslateUrl($doNotTranslateUrl)
    {
        $this->doNotTranslateUrl = $doNotTranslateUrl ? true : false;
    }
    /**
     * Sets the Translation Type to be used.<br />
     * The only implemented option is the
     * <code>self::YELVARO_MULTILINGUAL_CURRENT_PAGE</code> flag.
     *
     * @param   integer     $translateType      Use one of the
     *                                          <code>YELVARO_MULTILINGUAL_YELVARO</code>
     *                                          constants.
     * @throws  InvalidArgumentException
     */
    public function setTranslateType($translateType)
    {
        if (!is_int($translateType)) {
            $msg = t('The submitted type needs to be "integer", found "%s".', gettype($translateType));
            throw new InvalidArgumentException($msg);
        }
        $lowerLimit = 0;
        $upperLimit = 3;
        if ($translateType < $lowerLimit || $translateType > $upperLimit) {
            $msg = t(
                'The submitted value needs to be between $1%s and $2%s, found $3%s',
                $lowerLimit,
                $upperLimit,
                $translateType
            );
            throw new InvalidArgumentException($msg);
        }

        $this->translateType = $translateType;
    }
}
