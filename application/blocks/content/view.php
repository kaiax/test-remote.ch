<?php
defined('C5_EXECUTE') or die("Access Denied.");

$c = Page::getCurrentPage();
if (!$content && is_object($c) && $c->isEditMode()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Content Block.') ?></div>
    <?php
} else {

    // wrap all table tags in <div class="table-responsive">
    $content = preg_replace_callback('/<table/', function($matches) {
        return '<div class="table-responsive"><table';
    }, $content);

    // Table tag that has class attribute
    $content = preg_replace('/<table(.*)class="(.*)"/', '<table$1class="table $2"', $content);
    // Table tag that hasn't class attribute
    $content = preg_replace('#<table(?!(.*)(class))+(.*?)>#', '<table $3 class="table">',$content);
    $content = preg_replace_callback('/<\/table>/', function($matches) {
        return '</table></div>';
    }, $content);

    // obfuscate mail links
    if (version_compare(Config::get('concrete.version'), '8.0.0') == -1)
    {
        $ob = new Application\Src\Mail\Service\Obfuscator();
    } else {
        $ob = new Application\Mail\Service\Obfuscator();
    }

    $content = $ob->obfuscate($content);

    print '<div class="ccm-content-block">'.$content.'</div>';
}