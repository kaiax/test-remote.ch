<?php

namespace Application\Block\Content;

use Concrete\Block\Content\Controller as ContentBlockController;

/**
 * We extend the default Content Block controller and overwrride the Cache
 */
class Controller extends ContentBlockController
{
    protected $btCacheBlockRecord = false;
    protected $btCacheBlockOutput = false;
    protected $btCacheBlockOutputOnPost = false;
}
