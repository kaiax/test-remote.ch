<?php defined('C5_EXECUTE') or die('Access Denied.');
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$im = $app->make('helper/image');

if (is_object($f) && $f->getFileID()) {
    $thumb_xs = $im->getThumbnail($f, 768,  384, true); // 2:1  - 12:6
    $thumb_sm = $im->getThumbnail($f, 992,  397, true); // 5:2  - 15:6
    $thumb_md = $im->getThumbnail($f, 1200, 400, true); // 3:1  - 18:6
    $thumb_lg = $im->getThumbnail($f, 1600, 480, true); // 10:3 - 20:6
    $thumb_xl = $im->getThumbnail($f, 2000, 500, true); // 4:1  - 24:6
    ?>
    <div class="full-width-image">
    </div>
    <script>
        $(function(){
            $('.full-width-image').backstretch([
                [
                    { width: 768,  url: "<?=$thumb_xs->src?>"},
                    { width: 992,  url: "<?=$thumb_sm->src?>"},
                    { width: 1200, url: "<?=$thumb_md->src?>"},
                    { width: 1600, url: "<?=$thumb_lg->src?>"},
                    { width: 2000, url: "<?=$thumb_xl->src?>"}
                ]
            ]);
        })
    </script>
    <?php
} elseif ($c->isEditMode()) { ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Image Block.'); ?></div>
    <?php
}
