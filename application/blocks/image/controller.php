<?php
namespace Application\Block\Image;

use Concrete\Block\Image\Controller as ImageBlockController;

class Controller extends ImageBlockController
{
    protected $btCacheBlockOutput = false;
}
