<?php  defined('C5_EXECUTE') or die("Access Denied.");
$f = $controller->getFileObject();
$fp = new Permissions($f);
if ($f && $fp->canViewFile()) {
    $c = Page::getCurrentPage();
    if ($c instanceof Page) {
        $cID = $c->getCollectionID();
    }

    ?>

    <div class="ccm-block-file">
        <a href="<?php echo $forceDownload ? $f->getForceDownloadURL() : $f->getDownloadURL(); ?>" class="media">
            <div class="media-left">
                <svg class="media-object" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 42" width="32" height="42">
                    <path d="M20.8,1.5l9.7,9.8v29.2h-29v-39H20.8z"/>
                    <line x1="16" y1="12" x2="16" y2="30"/>
                    <polyline points="20,26 16,30 12,26 "/>
                </svg>
            </div>
            <div class="media-body">
                <p><?php echo stripslashes($controller->getLinkText()) ?></p>
                <p class="text-muted"><?= strtoupper($f->getExtension()) ?> (<?= $f->getSize(); ?>)</p>
            </div>
        </a>
    </div>


<?php
}

$c = Page::getCurrentPage();
 if (!$f && $c->isEditMode()) {
     ?>
    <div class="ccm-edit-mode-disabled-item"><?=t('Empty File Block.')?></div>
<?php
 }
