<?php defined('C5_EXECUTE') or die("Access denied.");

use \Concrete\Core\User\User;
use Concrete\Core\Block\BlockController;
use Concrete\Core\Tree\Node\Type\Topic;
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();


$c = Page::getCurrentPage();
$title = $c->getCollectionName();

?>


<div class="page-title-with-line">
    <div class="page-title-border-bottom">
        <div class="block">
            <div class="t-c">
                <h2 class="page-title-page-title">
                    <?= (! empty($cstmTitle = $controller->getTitleText())) ? $cstmTitle : $title ?>
                </h2>
            </div>
        </div>
    </div>
</div>
