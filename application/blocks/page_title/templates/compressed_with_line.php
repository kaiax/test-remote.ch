<?php defined('C5_EXECUTE') or die("Access denied.");

use \Concrete\Core\User\User;
use Concrete\Core\Block\BlockController;
use Concrete\Core\Tree\Node\Type\Topic;
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();


$c = Page::getCurrentPage();
$title = $c->getCollectionName();

?>


<div class="page-title-compressed-with-line">
    <div class="title-container">
        <h2><?= (! empty($cstmTitle = $controller->getTitleText())) ? $cstmTitle : $title ?></h2>
    </div>
</div>
