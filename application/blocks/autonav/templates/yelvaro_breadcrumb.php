<?php defined('C5_EXECUTE') or die("Access Denied.");
$navItems = $controller->getNavItems(true);?>

<ol class="breadcrumb">
<?php foreach($navItems as $key => $ni) :
    // Skip lowest Level when Multilanguage is used
    if(Package::getByHandle('multilingual') && $key == 1){
        continue;
    }
?>

    <?php if($ni->isCurrent): ?>
        <li class="active"><?=$ni->name?></li>
    <?php else: ?>
        <li><a href="<?=$ni->url?>"><?=$ni->name?></a></li>
    <?php endif;?>

<?php endforeach; ?>
</ol>