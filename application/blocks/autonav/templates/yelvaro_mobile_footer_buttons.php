<?php  defined('C5_EXECUTE') or die("Access Denied.");
/**
 * Mobile Footer Navigation
 * *************************
 * for this template to work as intended the autonav Block needs to be set to the following settings:
 * order_By: display_asc
 * displayPages: current
 * displaySubPages: relevant
 * displaySubPageLevels: enough_plus1
 */
$navItems = $controller->getNavItems();
$currentNavItem = '';
/*** STEP 1 of 2: Determine all CSS classes (only 2 are enabled by default, but you can un-comment other ones or add your own) ***/
foreach ($navItems as $ni) {
    $classes = array();

    if ($ni->isCurrent) {
        //class for the page currently being viewed
        $classes[] = 'nav-selected active';
        $currentNavItem = $ni;
    }

    if ($ni->inPath) {
        //class for parent items of the page currently being viewed
        $classes[] = 'nav-path-selected active';
    }

    //unique class for every single menu item
    $classes[] = 'nav-item-' . $ni->cID;

    //Put all classes together into one space-separated string
    $ni->classes = implode(" ", $classes);
}
//*** Step 2 of 2: Output menu HTML ***/
?>

<div class="footer-nav hidden-print hidden-sm hidden-md hidden-lg" role="navigation">
    <div class="row">
        <?php foreach ($navItems as $ni):
            if($ni->level <= $currentNavItem->level)continue; ?>
                <div class="col-sm-6">
                    <a href="<?=$ni->url?>" target="<?=$ni->target?>" data-level="<?=$ni->level?>" class="btn btn-primary <?=$ni->classes?>" style="width: 100%; margin-bottom: 20px;"><?=$ni->name?></a>
                </div>
            <?php endforeach;?>

        <?php // Display Back and Home Buttons when not on Start
        if(!$currentNavItem->isHome AND !$currentNavItem->cID != 1 AND $currentNavItem->name != 'de' AND $currentNavItem->url != 'fr' AND $currentNavItem->url != 'en' AND $currentNavItem->url != 'it'): ?>
            <div class="col-xs-6">
                <a href="./" class="btn btn-default <?=$ni->classes?>" style="width: 100%; margin-bottom: 20px;">Zurück</a>
            </div>
            <div class="col-xs-6">
                <a href="<?=DIR_REL?>/<?=Localization::activeLanguage();?>" class="btn btn-default <?=$ni->classes?>" style="width: 100%; margin-bottom: 20px;">Home</a>
            </div>
        <?php endif;?>
    </div>
</div>
