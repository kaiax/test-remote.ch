<?php defined('C5_EXECUTE') or die("Access Denied.");
$navItems = $controller->getNavItems(true);?>

<div id="breadcrumbs" class="btn-group btn-breadcrumb">
    <?php foreach($navItems as $key => $ni) :
        // Skip lowest Level when Multilanguage is used
        if(Package::getByHandle('multilingual') && $key == 1){
            continue;
        }
        if($ni->isHome): ?>
    
            <a href="<?=DIR_REL?>/" class="btn btn-default"><?=t('Home')?></a>
            <div class="btn btn-default">...</div>
            
        <?php elseif($ni->isCurrent): ?>
            
            <a class="btn btn-default"><div><?=$ni->name?></div></a>
            
        <?php else: ?>
            
            <a href="<?=$ni->url?>" class="btn btn-default"><div><?=$ni->name?></div></a>
            
        <?php endif;?>
    <?php endforeach; ?>
</div>



<script>
$(document).ready(function(){
    ellipses1 = $("#breadcrumbs :nth-child(2)")
    if ($("#breadcrumbs a:hidden").length >0) {ellipses1.show()} else {ellipses1.hide()}
    $(window).resize(function() {
        ellipses1 = $("#breadcrumbs :nth-child(2)")
        if ($("#breadcrumbs a:hidden").length >0) {ellipses1.show()} else {ellipses1.hide()}
    })
});
</script>

