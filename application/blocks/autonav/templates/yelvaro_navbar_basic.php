<?php
defined('C5_EXECUTE') or die("Access Denied.");

$navItems = $controller->getNavItems();
$c = Page::getCurrentPage();

foreach ($navItems as $ni) {
    $classes = array();

    if ($ni->url === $c->getCollectionPath() && $ni->name === 'Home') {
        // make sure the home is marked as active if needed
        $ni->inPath = 1;
        $ni->isCurrent = 1;
        $ni->isHome = 1;
    }

    if ($ni->isCurrent) {
        //class for the page currently being viewed
        $classes[] = 'nav-selected active';
    }

    if ($ni->inPath) {
        //class for parent items of the page currently being viewed
        $classes[] = 'nav-path-selected active';
    }

    //unique class for every single menu item
    $classes[] = 'nav-item-' . $ni->cID;

    //Put all classes together into one space-separated string
    $ni->classes = implode(" ", $classes);
}

// Bootstrap columns can be defined with the elements .navbar-wrapper and
// .navbar-wrapper-inner. Go to the css/project/organisms/navbars.less to adjust
// their mixins (.row, .make-sm-column(12), .make-md-column(8), .make-md-column-offset(2)).
// New breakpoints and mixins can be added to.
?>
<div class="navbar default-basic" role="navigation">
    <div class="navbar-wrapper">
        <div class="navbar-wrapper-inner">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                </button>
                <a class="navbar-brand" href="/"></a>
            </div>
            <div class="navbar-collapse collapse centered">
                <ul class="nav navbar-nav">
                    <?php foreach ($navItems as $ni): ?>
                        <li class="<?= $ni->classes ?>">
                            <a href="<?= $ni->url ?>" target="<?= $ni->target ?>" class="<?= $ni->classes ?>"><?= $ni->name ?></a>
                            <?php if ($ni->hasSubmenu) { //opens a dropdown sub-menu ?>
                                <ul class="visible-xs">
                            <?php } else { //closes a nav item ?>
                                </li>
                                <?php
                                //closes dropdown sub-menu(s) and their top-level nav item(s)
                                echo str_repeat('</ul></li>', $ni->subDepth);
                            } ?>
                    <?php endforeach; ?>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div><?php // The following element is just a placeholder for the fixed navigation.
            // It is only visible on XS. ?>
<div class="navbar-default-basic-spacer"></div>
