<?php
defined('C5_EXECUTE') or die('Access Denied.');

/* @var Concrete\Core\Form\Service\Form $form */
/* @var Concrete\Core\Block\View\BlockView $view */

// The text label as configured by the user
/* @var string $label */

// Array keys are the multilingual section IDs, array values are the language names (in their own language)
/* @var array $languages */

// The list of multilanguage language sections
/* @var Concrete\Core\Multilingual\Page\Section\Section[] $languageSections */

// The ID of the currently active multilingual section (if available)
/* @var int|null $activeLanguage */

// The default multilingual section
/* @var Concrete\Core\Multilingual\Page\Section\Section $defaultLocale */

// The current language code (without Country code)
/* @var string $locale */

// The ID of the current page
/* @var int $cID */
$langOptions = [];
$currentPage = Page::getByID($cID);
foreach($languageSections as $ls){
    $targetPageID = $ls->getTranslatedPageID($currentPage);
    if(!$targetPageID){
        $targetPage = Page::getByID($ls->getSiteHomePageID());
        $langOptionsAlt[$ls->getLanguageText()] = $targetPage->getCollectionLink();
    } else {
        $targetPage = Page::getByID($targetPageID);
        $langOptionsAlt[$ls->getLanguageText()] = $targetPage->getCollectionLink();
    }
    if(Localization::activeLanguage() == $ls->getLanguage()){
        $activeLanguageTitle = $ls->getLanguageText();
    }
}

if(empty($activeLanguageTitle))$activeLanguageTitle = 'Deutsch';

?>

<ul class="nav navbar-nav navbar-lang navbar-right">
    <li class="dropdown">
        <a href="" class="dropdown-toggle" data-toggle="dropdown"><span><?=$activeLanguageTitle?></span></a>
        <ul class="dropdown-menu dropdown-menu-right">
            <?php foreach($langOptionsAlt as $key => $lo):
                if($activeLanguageTitle == $key)continue;?>
            <li class="dropdown-item">
                <a href="<?=$lo?>"><span><?=$key?></span></a>
            </li>
            <?php endforeach; ?>
        </ul>
    </li>
</ul>