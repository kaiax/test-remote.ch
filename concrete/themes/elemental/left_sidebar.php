<?php
defined('C5_EXECUTE') or die("Access Denied.");
// get Current Page Informations
$c = Page::getCurrentPage();
$this->inc('elements/header.php');
?>

<main>
    <?php
    $a = new Area('Page Header');
    $a->display($c);
    ?>

    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sidebar">
                <?php
                $a = new Area('Sidebar');
                $a->setAreaGridMaximumColumns(12);
                $a->display($c);
                ?>
            </div>
            <div class="col-md-8 col-sm-offset-1 col-content">
                <?php
                $a = new Area('Main');
                $a->display($c);
                ?>
            </div>
        </div>
    </div>

</main>

<?php
$this->inc('elements/footer.php');
