<?php
defined('C5_EXECUTE') or die("Access Denied.");
// get Current Page Informations
$c = Page::getCurrentPage();
$this->inc('elements/header_top.php');
?>

<main>
    <?php
    $a = new Area('Main');
    $a->display($c);
    ?>
</main>

<?php
$this->inc('elements/footer_bottom.php');
