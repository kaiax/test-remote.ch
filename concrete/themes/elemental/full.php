<?php
defined('C5_EXECUTE') or die("Access Denied.");
// get Current Page Informations
$c = Page::getCurrentPage();
$this->inc('elements/header.php');
?>

<main>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <?php
                $a = new Area('Main Left');
                $a->setAreaGridMaximumColumns(12);
                $a->display($c);
                ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <?php
                $a = new Area('Main Middle');
                $a->display($c);
                ?>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <?php
                $a = new Area('Main Right');
                $a->display($c);
                ?>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php
                $a = new Area('Main Bottom');
                $a->display($c);
                ?>
            </div>
        </div>
    </div>
</main>

<?php
$this->inc('elements/footer.php');
